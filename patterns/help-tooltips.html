---
layout: default
secondary_menu_id: help-tooltips
primary_menu_id: patterns
title: Help, Tooltips
sketch_file: /downloads/patterns/help_tooltips.sketch
sketch_file_version: 0.4
summary: I need short, contextual information to assist or provide context.
tags: help, icon, icons, tooltip, tooltips, tool, tip, tips, tool tip, tooltips, tool tips, inline, text, content, static, assistance
---

<p class='summary'>{{ page.summary }}</p>
<div class='content-tab'>
  <ul class='tab-menu'>
    <li class='selected' data-target-tabs='tabs-1'>Design</li>
    <li data-target-tabs='tabs-3'>Guidance</li>
  </ul>
</div>
<div class='secondary-tab visible' id='tabs-1'>
  <div class='container'>
    <h2>Tooltips</h2>
    <p><span class='emphasis-term'>Most subtle technique.</span> Typically triggered by the user hovering on an icon, displaying a single, short sentence. Useful for explaining the difference between commands represented with icons, for example, batch table actions. Bear in mind that tooltips may not work well (or at all) on mobile devices, so alternatives may be needed.</p>
    <div class="pod">
      <div class='dls-form'>
        <div class='label-input-wrapper'>
          <label for="field-27">IBAN
            <div class='tooltip-icon'><i class="icon icon-info input-text-color-icon"></i>
              <div class='tooltip-container general-tooltip'>
                <div class='tooltip-pointer'>
                </div>
                <div class='tooltip-body general-tooltip'>International Bank Account Number - usually on your bank statement.
                </div>
              </div>
            </div>
          </label>
          <div class='dls-input-container input-height-m input-width-l'>
            <input type='text' class='dls-input input-small-biz' id='field-27' value=''/>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class='container'>
    <h2>Question and Answer</h2>
    <p><span class='emphasis-term'>A less subtle technique.</span> Linked common questions can be positioned on the page to tackle burning questions the majority of users may have, identify unfamiliar data, or reassure about collecting sensitive data.</p>
    <div class="pod">
      <div class='input-inline-wrapper'>
        <label for="field-34">Date of Birth</label>
        <div class='dls-input-container input-height-m input-width-xs input-inline'>
          <input type='text' class='dls-input input-small-biz' id='field-34' value='01'/>
        </div>
        <div class='dls-input-container input-width-xs input-height-m input-inline'>
          <input type='text' class='dls-input input-small-biz ' id='field-35' value='01'/>
        </div>
        <div class='dls-input-container input-height-m input-inline'>
          <input type='text' class='dls-input input-small-biz ' id='field-36' value='1981' style="width: 58px"/>
        </div>
        <div class='dls-icon-link'><a href='https://help.sageone.com/en_uk/accounting/extra-error-corrections.html' target='_blank'><i class="icon icon-question"></i><span>Why do we need this?</span></a>
        </div>
      </div>
    </div>
  </div>
  <div class='container'>
    <h2>Overt Help</h2>
    <p><span class='emphasis-term'>The least subtle technique.</span> Overtly placing helpful text on the UI can interject important information into the user’s flow, without the interruption of triggering a dialog. But, take care to avoid overwhelming the user with content.</p>
    <div class="pod">
      <div class='dls-form'>
        <div class='label-input-wrapper'>
          <label for="field-27">National Insurance Number</label>
          <p>Example: QQ123456A - On your employee's National Insurance Card or P45.</p>
          <div class='dls-input-container input-height-m input-width-l'>
            <input type='text' class='dls-input input-small-biz' id='field-27'/>
          </div>
        </div>
      </div>
    </div>
    <div class='container'>
      <h2>Which Icon?</h2>
      <p>For more info, see the foundation on <a href='/foundations/icons/'>Icons</a>.</p>
      <div class="pod">
        <table class='pod-table'>
          <tr>
            <td>
              <div class='tooltip-icon'><i class="icon icon-info input-text-color-icon"></i>
                <div class='tooltip-container general-tooltip'>
                  <div class='tooltip-pointer'>
                  </div>
                  <div class='tooltip-body general-tooltip'>This is an example of a tooltip.
                  </div>
                </div>
              </div>
            </td>
            <td>
              For a tooltip which shows on hover, in context.
            </td>
          </tr>
        </div>
        <tr>
          <td>
            <div class='dls-icon-link'><a href='https://help.sageone.com/en_uk/accounting/extra-error-corrections.html' target='_blank'><i class="icon icon-question input-text-color-icon"></i></a>
            </div>
          </td>
          <td>
            For a link to a help article which loads in a new tab.
          </td>
        </tr>
        <tr>
          <td>
            <div class='dls-icon-link'><a href='https://help.sageone.com/en_uk/accounting/extra-error-corrections.html' target='_blank'><i class="icon icon-link input-text-color-icon"></i></a>
            </div>
          </td>
          <td>
            For a link to a non-Sage website which loads in a new tab.
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>
</div>
<div class='secondary-tab' id='tabs-3'>
  <div class='container'>
    <h2>Have I got the right component?</h2>
    <ul class="standard-list">
      <li>Important enough to prevent user activity before something is actioned or acknowledged? Try a <a href='/patterns/dialogs/'>Dialog</a>.</li>
      <li>Relates to error, warning, or info states? See the <a href='/components/text-inputs'>Text Inputs Component</a> and <a href='/patterns/errors-validation/'>Errors and Validation Pattern</a>.</li>
      <li>For the icon font, and guidance on icon colour, see the <a href='/foundations/illustration/icons/'>Foundation on Icons</a>.</li>
    </ul>
  </div>
  <div class='container'>
    <h2>Guidance, hints and tips</h2>
    <ul class="standard-list">
      <li>We tend to avoid dynamic inline help (content appearing dynamically on the UI when the user focuses on a whole field, rather than an icon), because accessibility requirements urge caution with substantial UI changes on focus, and it can distract the user from completion.  It might still be useful in some situations though.</li>
      <li>We tend to opt for user-activated inline help instead, since it means help is available to the users, consistently, every time they need it and at the time they need it, but not unless they need it.</li>
      <li>But it can mask other UI elements or field labels. Take care to limit the size of tooltips (primarily by keeping wording small), and use top/bottom, and left/right positioning of the tooltip body to best effect.</li>
      <li>When implementing tooltips, consider a short mouseover delay (e.g. 500 ms) to avoid accidentally triggering the popup if a user didn't intend to.</li>
      <li>Keep content short and simple - users are unlikely to read anything longer than a line.</li>
      <li>Focus on the action the user must do - make it relevant to their current situation, and avoid giving background information.</li>
      <li>Don’t show more than one tooltip at a time. Animate or transition tooltips in and out.</li>
    </ul>
  </div>
</div>
