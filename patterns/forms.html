---
layout: default
secondary_menu_id: forms
primary_menu_id: patterns
title: Forms
sketch_file: /downloads/patterns/forms.sketch
sketch_file_version: 0.3
summary: I need to gather data to accomplish frequent and significant tasks - like signing up, recording a customer, or creating an invoice.
tags: form, forms, input, inputs, button, buttons, label, labels, margin, margins, padding
---

<p class='summary'>{{ page.summary }}</p>
<div class='content-tab'>
  <ul class='tab-menu'>
    <li class='selected' data-target-tabs='tabs-1'>Design</li>
    <li data-target-tabs='tabs-3'>Guidance</li>
  </ul>
</div>
<div class='secondary-tab visible' id='tabs-1'>
  <div class='container'>
    <p><a href='https://www.lukew.com/resources/web_form_design.asp' target='_blank'>Luke Wroblewski’s guidance on web forms</a> is a great source of best practice. Check out the <a href='/components/text-inputs/'>design system guidance on text inputs</a> too. There's also a useful <a href='https://sage365-my.sharepoint.com/:v:/g/personal/ben_wilson_sage_com/EYaLaOBASOdGnGzaZ5TCe9sB-SENDUr9-S2jiMzZO5_QFQ?e=jWyNQc'>talk from back in 2015</a> - whilst the design patterns have evolved, the principles hold true.</p>
    <ul class="standard-list">
      <li>Long forms with scrolling are OK if the form is all about the same topic, and there is inline dynamic validation. Otherwise, paginate the form with a <a href='/components/step-flows/'>Step Flow</a>.</li>
      <li>Create a clear ‘path to completion’ - a strong vertical axis of labels, input fields, and the primary action.
        <ul class="standard-list">
          <li>For single-use and sign-up forms, place your primary action in the 'path to completion' - positioned left under the last input.</li>
          <li>For dialogs and in-app forms, buttons are usually positioned at the bottom right. In a pair, Cancel goes on the left, and Save goes on the right.</li>
          <li>Consider consistency of experience too - stick to the best method for your users, unless part of a one-time experience.</li>
        </ul>
      </li>
      <li>Don’t use more than one column of fields if you can help it.</li>
      <li>Use <span class='emphasis-term'>input width affordance</span> - small fields for small data (e.g. postal codes), large fields for large data (e.g. email addresses).</li>
      <li>Our first choice for field label positioning is top aligned, because users complete forms faster this way.
        <ul class="standard-list">
          <li>Single-use or sign-up forms might use field labels that animate from within the field, to top aligned.</li>
          <li>Keep form labels as short as possible - preferably 28 characters or less. Use techniques from the <a href='/patterns/help-tooltips'>Help, Tooltips</a> pattern to keep length short.</li>
        </ul>
      </li>
      <li>Consider whether there's a benefit in indicating optional or mandatory fields. If you decide to, indicate whichever is rarer, not both.</li>
      <li>Use <span class='emphasis-term'>smart defaults</span> - if a clear majority of users would make a given selection, set this selection by default.</li>
    </ul>
  </div>
  <div class='container'>
    <h2>Top-Aligned Labels are Best</h2>
    <p>Completion is 10x as fast as left-aligned. But your form is longer.</p>
    <ul class="standard-list">
      <li>Vertical space between bottom of field border and top of next field label: 32px</li>
      <li>Vertical space between bottom of field label and top of field border: 12px</li>
      <li>Vertical space between last input and buttons: 48px</li>
      <li>Horizontal and vertical pacing between buttons and container: 32px</li>
      <li>Horizontal spacing between buttons: 16px</li>
    </ul>
    <div class="system-message info-message">
      <div class='system-message-title'>Spacing and Sizing</div>
      <div class='system-message-body'>The Design System proposes more generous spacing and sizing. If implementing is difficult, try a different <a href='/components/text-inputs'>field height</a>. It's OK to use a legacy regime as long as its consistent within your product, and supported with rationale.</div>
    </div>
    <div class='pod'>
      <h3>Single-Use or Sign-up Form Example</h3>
      <div class='dls-form'>
        <div class='label-input-wrapper'>
          <label for="field-1">First Name</label>
          <div class='dls-input-container input-height-m input-width-l'>
            <input type='text' class='dls-input input-small-biz' id='field-1' value=''/>
          </div>
        </div>
        <div class='label-input-wrapper'>
          <label for="field-2">Last Name</label>
          <div class='dls-input-container input-height-m input-width-l'>
            <input type='text' class='dls-input input-small-biz' id='field-2' value=''/>
          </div>
        </div>
        <div class='input-inline-wrapper'>
          <label for="field-34">Date of Birth</label>
          <div class='dls-input-container input-height-m input-width-xs input-inline'>
            <input type='text' class='dls-input input-small-biz' id='field-34' placeholder='DD'/>
          </div>
          <div class='dls-input-container input-width-xs input-height-m input-inline'>
            <input type='text' class='dls-input input-small-biz ' id='field-35' placeholder='MM'/>
          </div>
          <div class='dls-input-container input-height-m input-inline'>
            <input type='text' class='dls-input input-small-biz ' id='field-36' placeholder='YYYY' style="width: 58px"/>
          </div>
          <div class='dls-icon-link'><a href='https://help.sageone.com/en_uk/accounting/extra-error-corrections.html' target='_blank'><i class="icon icon-question"></i><span>Why do we need this?</span></a>
          </div>
          <div class='label-input-wrapper'>
            <label for="field-6">Email</label>
            <div class='dls-input-container input-height-m input-width-l'>
              <input type='text' class='dls-input input-small-biz' id='field-6' value=''/>
            </div>
          </div>
          <div class='label-input-wrapper'>
            <label for="field-7">Address 1</label>
            <div class='dls-input-container input-height-m input-width-l'>
              <input type='text' class='dls-input input-small-biz' id='field-7' value=''/>
            </div>
          </div>
          <div class='label-input-wrapper'>
            <label for="field-8">Address 2 <span class='optional'>(optional)</span></label>
            <div class='dls-input-container input-height-m input-width-l'>
              <input type='text' class='dls-input input-small-biz' id='field-8' value=''/>
            </div>
          </div>
          <div class='label-input-wrapper'>
            <label for="field-9">Town/City</label>
            <div class='dls-input-container input-height-m input-width-l'>
              <input type='text' class='dls-input input-small-biz' id='field-9' value=''/>
            </div>
          </div>
          <div class='label-input-wrapper'>
            <label for="field-10">Postcode</label>
            <div class='dls-input-container input-height-m input-width-m'>
              <input type='text' class='dls-input input-small-biz' id='field-10' value=''/>
            </div>
          </div>
          <div class='dls-form-button-container-left'>
            <button class='dls-button button-medium small-biz primary'>Save</button><button class='dls-button button-small small-biz tertiary'>Cancel</button>
          </div>
        </div>
      </div>
      <div class='example-dialog'>
        <div class="header-section">
          <h3>Dialog Example</h3>
          <div class='closebox'><i class="icon icon-close dialog-close"></i></div>
        </div>
        <div class="dialog-content">
          <div class='dls-form'>
            <div class='label-input-wrapper'>
              <label for="field-11">First Name</label>
              <div class='dls-input-container input-height-m input-width-l'>
                <input type='text' class='dls-input input-small-biz' id='field-11' value=''/>
              </div>
            </div>
            <div class='label-input-wrapper'>
              <label for="field-12">Last Name</label>
              <div class='dls-input-container input-height-m input-width-l'>
                <input type='text' class='dls-input input-small-biz' id='field-12' value=''/>
              </div>
            </div>
            <div class='label-input-wrapper'>
             <label for="field-13">NI Number</label>
             <div class='dls-input-container input-height-m input-width-l'>
              <input type='text' class='dls-input input-small-biz force-focus' id='field-13' placeholder='e.g. QQ123456A'/>
            </div>
          </div>
          <div class='dls-form-button-container-right'>
            <button class='dls-button button-small small-biz tertiary'>Cancel</button><button class='dls-button button-medium small-biz primary'>Save</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class='container'>
    <h2>But Right-Aligned Labels Can Work Too</h2>
    <p>Completion is 2x as fast as left-aligned - less vertical height, but less room for labels.</p>
    <ul class="standard-list">
      <li>Vertical space between adjacent field borders: 32px</li>
      <li>Horizontal space between field label and left field border: 12px</li>
      <li>Vertical space between last input and buttons: 48px</li>
      <li>Horizontal and vertical pacing between buttons and container: 32px</li>
      <li>Horizontal spacing between buttons: 16px</li>
    </ul>
    <div class='pod'>
      <h3>Single-Use or Sign-up Form Example</h3>
      <div class='dls-form form-right-aligned'>
        <div class='label-input-wrapper'>
          <label for="field-14">First Name</label>
          <div class='dls-input-container input-height-m input-width-l'>
            <input type='text' class='dls-input input-small-biz' id='field-14' value=''/>
          </div>
        </div>
        <div class='label-input-wrapper'>
          <label for="field-15">Last Name</label>
          <div class='dls-input-container input-height-m input-width-l'>
            <input type='text' class='dls-input input-small-biz' id='field-15' value=''/>
          </div>
        </div>
        <div class='label-input-wrapper input-inline-wrapper'>
          <label for="field-34">Date of Birth</label>
          <div class='dls-input-container input-height-m input-width-xs input-inline'>
            <input type='text' class='dls-input input-small-biz' id='field-34' placeholder='DD'/>
          </div>
          <div class='dls-input-container input-width-xs input-height-m input-inline'>
            <input type='text' class='dls-input input-small-biz ' id='field-35' placeholder='MM'/>
          </div>
          <div class='dls-input-container input-height-m input-inline'>
            <input type='text' class='dls-input input-small-biz ' id='field-36' placeholder='YYYY' style="width: 58px"/>
          </div>
          <div class='dls-icon-link'><a href='https://help.sageone.com/en_uk/accounting/extra-error-corrections.html' target='_blank'><i class="icon icon-question"></i><span>Why do we need this?</span></a>
          </div>
        </div>
        <div class='label-input-wrapper'>
          <label for="field-19">Email</label>
          <div class='dls-input-container input-height-m input-width-l'>
            <input type='text' class='dls-input input-small-biz' id='field-19' value=''/>
          </div>
        </div>
        <div class='label-input-wrapper'>
          <label for="field-20">Address 1</label>
          <div class='dls-input-container input-height-m input-width-l'>
            <input type='text' class='dls-input input-small-biz' id='field-20' value=''/>
          </div>
        </div>
        <div class='label-input-wrapper'>
          <label for="field-21">Address 2 <span class='optional'>(optional)</span></label>
          <div class='dls-input-container input-height-m input-width-l'>
            <input type='text' class='dls-input input-small-biz' id='field-21' value=''/>
          </div>
        </div>
        <div class='label-input-wrapper'>
          <label for="field-22">Town/City</label>
          <div class='dls-input-container input-height-m input-width-l'>
            <input type='text' class='dls-input input-small-biz' id='field-22' value=''/>
          </div>
        </div>
        <div class='label-input-wrapper'>
          <label for="field-23">Postcode</label>
          <div class='dls-input-container input-height-m input-width-m'>
            <input type='text' class='dls-input input-small-biz' id='field-23' value=''/>
          </div>
        </div>
        <div class='dls-form-button-container-left'>
          <button class='dls-button button-medium small-biz primary'>Save</button><button class='dls-button button-small small-biz tertiary'>Cancel</button>
        </div>
      </div>
      <div class='example-dialog'>
        <div class="header-section">
          <h3>Dialog Example</h3>
          <div class='closebox'><i class="icon icon-close dialog-close"></i></div>
        </div>
        <div class="dialog-content">
          <div class='dls-form form-right-aligned'>
            <div class='label-input-wrapper'>
              <label for="field-24">First Name</label>
              <div class='dls-input-container input-height-m input-width-l'>
                <input type='text' class='dls-input input-small-biz' id='field-24' value=''/>
              </div>
            </div>
            <div class='label-input-wrapper'>
              <label for="field-25">Last Name</label>
              <div class='dls-input-container input-height-m input-width-l'>
                <input type='text' class='dls-input input-small-biz' id='field-25' value=''/>
              </div>
            </div>
            <div class='label-input-wrapper'>
              <label for="field-26">NI Number</label>
              <div class='dls-input-container input-height-m input-width-l'>
                <input type='text' class='dls-input input-small-biz force-focus' id='field-26' placeholder='e.g. QQ123456A'/>
              </div>
            </div>
            <div class='dls-form-button-container-right'>
              <button class='dls-button button-small small-biz tertiary'>Cancel</button><button class='dls-button button-medium small-biz primary'>Save</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div class='secondary-tab' id='tabs-3'>
  <div class='container'>
    <h2>Have I got the right pattern?</h2>
    <ul class="standard-list">
      <li>Need input fields to build your form? Check out: <a href='/components/checkboxes/'>Checkboxes</a>, <a href='/components/radios/'>Radios</a>, and <a href='/components/switches/'>Switches</a>, <a href='/components/datepicker/'>Datepickers</a>, <a href='/components/dropdowns/'>Dropdowns</a>, and <a href='/components/text-inputs/'>Text Inputs</a>.</li>
      <li>Ready to add actions to your form? See <a href='/components/buttons/'>Buttons</a></li>
      <li>Need to add user assistance? Try <a href='/patterns/help-tooltips/'>Help and Tooltips</a></li>
      <li>Design error recovery using the pattern for <a href='/patterns/errors-validation/'>Errors and Validation</a></li>
      <li>Long form that you'd like to split into pages? Try a <a href='/components/step-flows/'>Step Flow</a></li>
    </ul>
  </div>
  <div class='container'>
    <h2>What configurations are available?</h2>
    <ul class="standard-list">
      <li>Label alignment: Top aligned; Right aligned.</li>
    </ul>
  </div>
  <div class='container'>
    <h2>Structuring your form</h2>
    <ul class="standard-list">
      <li>Long forms with scrolling are OK if the form is all about the same topic, and there is inline dynamic validation. Otherwise, use natural topics breaks to paginate the form. Try a <a href='/components/step-flows/'>Step Flow</a>.</li>
      <li>Consider whether there's value in placing focus on the first empty field when the form loads.</li>
    </ul>
  </div>
  <div class='container'>
    <h2>Layout and Spacing</h2>
    <ul class="standard-list">
      <li>Create a clear scan line from start to finish - the ‘path to completion’ - a strong vertical axis of labels, input fields, and the primary action.</li>
      <li>For a 'medium' <a href='/components/text-inputs/'>input field</a> (40px height), leave a space of 32px between the bottom of the previous input field, and the top of the field label for the next (guidance typically suggests 50-75% the height of input field between each field).</li>
      <li>Don’t use more than one column of fields if you can help it - this can make it difficult to understand the order of the form, and a column may be missed on small screens.</li>
      <li>Use <span class='emphasis-term'>input width affordance</span> - small fields for short data (e.g. postal codes), large fields for long data (e.g. email addresses).</li>
    </ul>
  </div>
  <div class='container'>
    <h2>Buttons</h2>
    <p>See <a href='/components/buttons/'> the button component</a> for more information.</p>
    <ul class="standard-list">
      <li>Every form should have a single primary action - (e.g. Save, Submit, Continue).</li>
      <li>Tertiary or ‘ghost’ buttons are used for reversing actions, like ‘Cancel’ or ‘Back’.</li>
      <li>The form title often matches the label on the primary action, and the label of the action that displayed the form.</li>
      <li><span class='emphasis-term'>For web apps:</span>
        <ul class="standard-list">
          <li><span class='emphasis-term'>Button Order:</span> In a button pair, 'Cancel' goes on the left, and 'Save' goes on the right - a consensus of surveyed third party Design Systems does it this way (with Windows being a rare exception). Although Nielsen (2008) says "(neither) choice is likely to cause usability catastrophes".</li>
          <li><span class='emphasis-term'>Button Positioning:</span> Almost all design systems place buttons at the bottom right of dialogs or pages. For languages with a left-to-right, top-to-bottom reading order, this means a dialog ends with its positive conclusion, but has the disadvantage that users must tab past 'Cancel' to reach the positive action.</li>
        </ul>
      </li>
      <li><span class='emphasis-term'>For single-use or sign-up forms, typically on websites</span>:
        <ul class="standard-list">
          <li><span class='emphasis-term'>Button Positioning:</span> Position buttons on the left of the page, so they align with the path to completion: Wroblewski (2007) says "primary actions directly aligned with input fields tend to increase completion rates".</li>
          <li><span class='emphasis-term'>Button Order:</span> Place 'Save' on the left to support the path to completion. Consider moving 'Cancel' away to avoid mistaken clicks.</li>
        </ul>
      </li>
      <li>But as always, what works, wins - you can break the rule if you have evidence or rationale that shows something else is best for your users.</li>
    </ul>
  </div>
  <div class='container'>
    <h2>Field labels</h2>
    <ul class="standard-list">
      <li><span class='emphasis-term'>First Choice</span>: Top-aligned labels are recommended because a single eye fixation can take in both the label and field. User eye fixation is 10 times faster than left aligned labels, and twice as fast as right-aligned labels. Top-aligned labels are also most suitable for languages requiring longer label length. However, top-aligned labels take up much more vertical space.</li>
      <li><span class='emphasis-term'>Second Choice</span>: Right-aligned labels. They perform better than left aligned labels, and reduce the vertical height of the form compared to top aligned labels. However, labels may wrap if long, which isn't always elegant.</li>
      <li><span class='emphasis-term'>Third Choice</span>: Left aligned labels are our least favoured choice, requiring two separate eye fixations.</li>
      <li>Labels within inputs are fashionable, but because the user overtypes the field label, the user has no context if they need to delete and re-enter an answer.</li>
      <li>But, animating the field label from within the input to above the input could solve this problem. This creates a smooth experience for single-use and sign-up forms. Some authors recommend this as a new standard, but it may become tedious or distracting if applied to all fields in a complex web app for example.</li>
      <li>Form labels have Initial Capital case to aid visual scanning. Examples: Customer Name, Payment Type.</li>
    </ul>
  </div>
  <div class='container'>
    <h2>Optional and Mandatory Fields</h2>
    <ul class="standard-list">
      <li>To avoid abandonment, don’t collect any data that isn't strictly needed. Optional fields should be very rare.</li>
      <li>In general, consider not indicating mandatory fields, because:
        <ul class="standard-list">
          <li>Usually they’re prolific on the page - users may visually screen indicators out as ‘noise’, and encounter error anyway.</li>
          <li>In UI that the user encounters often, users don’t always need to be reminded about mandatory fields.</li>
          <li>'Smart Default' content can be more useful, if users are likely to make a particular entry.</li>
        </ul>
      </li>
      <li>But, if you do wish to indicate mandatory fields, indicate the minority case:
        <ul class="standard-list">
          <li>If mandatory fields are rare, indicate them with an asterisk*</li>
          <li>If optional fields are rare, indicate them with the text “(optional)”.</li>
          <li>Do one or the other on a given page, not both.</li>
        </ul>
      </li>
    </ul>
  </div>
  <div class='container'>
    <h2>Flexible Input</h2>
    <ul class="standard-list">
      <li>Allow the user to enter data in various formats, rather than specifying required formats up front. For example:
        <ul class="standard-list">
          <li>Postcodes and credit card numbers with, and without spaces.</li>
          <li>Phone numbers with international prefixes, and without.</li>
          <li>Dates in a variety of formats (e.g. "01/04/2018", "1/4/18", "1/4", "1 Apr").</li>
        </ul>
      </li>
      <li>Correct the data silently on submission - not dynamically when a user is entering, or on loss of focus.</li>
    </ul>
  </div>
  <div class='container'>
    <h2>Smart Defaults</h2>
    <ul class="standard-list">
      <li>Rather than starting with an empty form, if a clear majority of users would make a given selection, set this selection by default.</li>
      <li>Because people are likely to leave them in place, ensure they align with most user’s goals (which may be different to commercial goals)!</li>
      <li>Carefully consider smart defaults with radio buttons, as they're particularly easily missed, and users may end up with bad data.</li>
    </ul>
  </div>
  <div class='container'>
    <h2>Help</h2>
    <p>See <a href='/patterns/help-tooltips/'> the help and tooltips component</a> for more information.</p>
  </div>
  <div class='container'>
    <h3>Placeholder Text</h3>
    <div class="system-message warning-message">
      <div class='system-message-title'>Accessibility and Placeholder Text
      </div>
      <div class='system-message-body'>We'll keep this topic under careful review.
      </div>
    </div>
    <ul class='standard-list'>
      <li>Usability and accessibility issues with placeholder text can be common. You might prefer to use alternative methods such as those outlined on the <a href='/patterns/help-tooltips/'>Help, tooltips</a> page, such as placing formatting examples as plain text beneath the field label.</li>
      <li>Placeholder text can give users a clue about required formats (for example, 'e.g. QQ123456A' in a UK National Insurance Number field).</li>
      <li>Where possible, use content that is clearly a placeholder (e.g. 'MM/YY'), rather than content that could be mistaken as pre-filled.</li>
      <li>Placeholder text is a 30% opacity of black (see <a href='/foundations/colour/'>Colours</a>).</li>
      <li>Technically speaking, this doesn’t give an accessible contrast ratio. However, some research shows that if we use the minimum accessible colour, it appears to many users with the same contrast as real content, so more users skip important fields and encounter errors. We'll keep this under careful review.</li>
    </ul>
  </div>
  <div class='container'>
    <h2>Progressive Disclosure, Additional or Selection Dependent Inputs</h2>
    <ul class="standard-list">
      <li>These techniques show additional fields depending on the user’s input, or clicking a command, for example, customising the form based on the answer to radio button, menu, or tab selection.</li>
      <li>Always try to display initial selections and selection dependent inputs in close proximity, and visually associated to each other. Consider separators, spacing, and group boxes.</li>
      <li>This does result in ‘page jumping’ – pushing the form down the page to accommodate additional content. Accessibility requirements urge caution with substantial UI changes on focus, and users may be confused if they lose their frame of reference. Consider using overlays or dialogs instead, but be cautious about obscuring other form details.</li>
    </ul>
  </div>
  <div class='container'>
    <h2>Collecting Personal Data</h2>
    <ul class="standard-list">
      <li>In general, collect a <span class='emphasis-term'>person's name</span> in separate fields - 'First Name' and 'Last Name', and an optional 'Middle Name' only if you need it.</li>
      <li>Don't collect names in a single 'Full Name' field, because you can’t then reliably extract parts of a name.</li>
      <li>Be sensitive to <a href='https://www.w3.org/International/questions/qa-personal-names' target='_blank'>regional and cultural practices with names</a>.</li>
      <li>Consider whether you really need to collect a <span class='emphasis-term'>person's title</span> - It’s extra work for them and you’re asking them to potentially reveal their gender and marital status, which they may not want to do.</li>
      <li>You should only ask users about <span class='emphasis-term'>gender or sex</span> if you genuinely can’t deliver your service without this information. Outside a medical context, use the term 'gender'.</li>
      <li>Always collect <span class='emphasis-term'>address elements</span> in separate fields. If you use a single textarea, you can’t then reliably extract parts of an address. Consider using an address lookup to save the user effort, and ensure quality.</li>
      <li>Collect <span class='emphasis-term'>dates of birth</span> using <a href='http://uxmovement.com/forms/bad-practices-on-birthdate-form-fields/' target='_blank'>three text inputs</a> for keyboard entry of day, month, and year numerals. Set the width of each input to give users a clue about its content, and use 'DD', 'MM', and 'YYYY' placeholders.</li>
      <li>For more on this topic, <a href='https://design-system.service.gov.uk/patterns/' target='_blank'>Gov.uk's design system</a> has useful guidance.</li>
    </ul>
  </div>
</div>
