---
layout: default
secondary_menu_id: grid
primary_menu_id: foundations
title: Grid System
sketch_file: /downloads/foundations/grid_system.sketch
sketch_file_version: 0.3
summary: The basic layout and visual rhythm of our apps and pages.
tags: breakpoint, breakpoints, grid, responsive, adaptive, screen size, columns, gutters, margins, column, gutter, margin, fluid, fixed
---

<p class='summary'>{{ page.summary }}</p>
<div class='container'>
  <h2>Grid and Layout for Websites</h2>
  <p>Page templates and module widths are particularly important to build compelling websites. Check out this more detailed guidance.</p>
  <div class="download-file">
    <a href="/downloads/foundations/website_grid.sketch" target="_blank" class="dls-button size-small business-generic secondary"><span>Templates and Layout for Websites</span> <i class="icon icon-download"></i></a>
  </div>
</div>
<div class='container'>
  <h2>Breakpoints for Product Design</h2>
  <ul class="standard-list">
    <li>Breakpoints define where the page layout should change to make best use of different screen sizes - as part of responsive or adaptive mobile design, or just when a user adjusts the size of their browser.</li>
    <li>We've reviewed analytics from our cloud products, and the sizes of the most popular devices, to arrive at the following breakpoints.</li>
    <li>According to Google Analytics (for Accounting, August 2018)*, if you design for the ‘Large’ size, then you’ll cater for 52% of users. So, you might decide to design for this size first (check out the table below for more on this).</li>
    <li>Then, design for ‘Extra Small’, which would catch the most popular iOS and Android devices, and satisfy accessibility guidelines.</li>
  </ul>
  <div class="system-message info-message">
    <div class='system-message-title'>WCAG Version 2.1 Update</div>
    <div class='system-message-body'>The latest WCAG standard introduced a requirement for <a href='/foundations/accessibility#accessibility-responsive'>responsive design down to 320px page width</a>.</div>
  </div>
  <p>After meeting accessibility guidelines, it's OK to choose not to design for each size, or to design for additional sizes, if this is the right thing to do for your users - check out your own analytics to make a decision.</p>
  <table class='dls-table'>
    <tr>
      <th>Breakpoints (<a href='#units'>measured in dp</a>)</th>
      <th>% of users*</th>
      <th>Device Type</th>
      <th>Columns</th>
      <th>Margins</th>
      <th>Gutters</th>
    </tr>
    <tr>
      <td>Extra Small (320-599px)</td>
      <td>3.6%</td>
      <td>Smart Phones</td>
      <td>6</td>
      <td>16px</td>
      <td>16px</td>
    </tr>
    <tr>
      <td>Small (600-959px)</td>
      <td>2.0%</td>
      <td>Portrait Tablets</td>
      <td>6</td>
      <td>24px</td>
      <td>16px</td>
    </tr>
    <tr>
      <td>Medium (960-1259px)</td>
      <td>1.9%</td>
      <td>Landscape Tablets &amp; Low-Res Laptops</td>
      <td>12</td>
      <td>32px</td>
      <td>24px</td>
    </tr>
    <tr>
      <td>Large (1260-1920px)</td>
      <td>52.1%</td>
      <td>High-Res Laptops &amp; Monitors</td>
      <td>12</td>
      <td>40px</td>
      <td>24px</td>
    </tr>
    <tr>
      <td>Extra Large (1921px and above)</td>
      <td>33.5%</td>
      <td>Ultra High-Res Monitors</td>
      <td>12 Fixed Width &amp; Left-aligned</td>
      <td>40px</td>
      <td>24px</td>
    </tr>
  </table>
</div>
<div class='container'>
  <h2>Columns, Gutters and Margins</h2>
  <p>Together, these provide a 'skeleton' to position and align design elements to. They're all based on the 8x multiplier, which we also use for things like <a href='/foundations/spatial/'>spatial markup</a>, and <a href='/foundations/spatial/'>typography</a> sizing. This sets up an elegant and harmonious feel.</p>
  <ul class="standard-list">
    <li><span class='emphasis-term'>Columns</span>: Provide regular intervals to align or size your design elements to.</li>
    <li><span class='emphasis-term'>Gutters</span>: The space between each column.</li>
    <li><span class='emphasis-term'>Margins</span>: The space between the edge of the screen, and the first and last columns.</li>
  </ul>
  <p>Choose a breakpoint to see an example of the grid, including columns, gutters, and margins.</p>
</div>
<div class='container'>
  <h2>Fluid or Fixed?</h2>
  <ul class="standard-list">
    <li>Within a breakpoint, if you increase the size of the browser, then the columns are fluid (they increase in size to take up the extra space), but margins and gutters are fixed (they always keep their original size).</li>
    <li>Since the Extra Large size is the biggest, for this breakpoint only, columns and gutters are fixed, but margins flex to take up any space.</li>
    <li>If you're using left navigation, then the left margin would be fixed, and right would flex. If you're using top navigation, then the content area would be centre-aligned, with flex margins on each side.</li>
  </ul>
  <div class="system-message info-message">
    <div class='system-message-title'>Grids for Modern Full Width Apps</div>
    <div class='system-message-body'>We don't yet have much experience implementing modern grid systems from scratch, so feel free to contribute your recommendations with rationale to the design system.</div>
  </div>
  <div class='content-tab'>
    <ul class='tab-menu full-width'>
      <li class='selected' data-target-tabs='tabs-1'>Extra Small</li>
      <li data-target-tabs='tabs-2'>Small</li>
      <li data-target-tabs='tabs-3'>Medium</li>
      <li data-target-tabs='tabs-4'>Large</li>
      <li data-target-tabs='tabs-5'>Extra Large</li>
    </ul>
  </div>
  <div class='secondary-tab visible' id='tabs-1'>
    <div class='pod fixed-height full-width'>
      <img src='/visuals/grid-xs.png'/>
    </div>
  </div>
  <div class='secondary-tab' id='tabs-2'>
    <div class='pod fixed-height full-width'>
      <img src='/visuals/grid-s.png'/>
    </div>
  </div>
  <div class='secondary-tab' id='tabs-3'>
    <div class='pod fixed-height full-width'>
      <img src='/visuals/grid-m.png'/>
    </div>
  </div>
  <div class='secondary-tab' id='tabs-4'>
    <div class='pod fixed-height full-width'>
      <img src='/visuals/grid-l.png'/>
    </div>
  </div>
  <div class='secondary-tab' id='tabs-5'>
    <div class='pod fixed-height full-width'>
      <img src='/visuals/grid-xl.png'/>
    </div>
  </div>
</div>
<div class='container'>
  <h2><a id='units'></a>Units of Measurement</h2>
  <p>Make sure you're measuring with the right units!</p>
  <ul class="standard-list">
    <li>Pixels (px) - actual pixels on the screen. We use this most of the time.</li>
    <li>Points (pt) - 1/72 of an inch based on the physical size of the screen. We don't tend to use this, but some accessibility standards do. It's easy to get mixed up.</li>
    <li>Density-independent Pixels (dp) - a unit based on the physical density of the screen, so it's universal across devices with different densities, like Apple Retina displays, for example.</li>
  </ul>
</div>
