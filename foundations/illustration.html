---
layout: default
secondary_menu_id: illustration
primary_menu_id: foundations
title: Illustration
summary: Bring Sage Business Cloud to life, with a fresh, vibrant, and unique style.
tags: illustration, illustrations, character, composition, characters, compositions
---
<p class='summary'>{{ page.summary }}</p>
<div class='content-tab'>
  <ul class='tab-menu'>
    <li class='selected' data-target-tabs='tabs-1'>Guidance</li>
    <li data-target-tabs='tabs-2'>Examples</li>
    <li data-target-tabs='tabs-3'>Colour Palette</li>
  </ul>
</div>
<div class='secondary-tab visible' id='tabs-1'>
  <h3>Illustration Guidance</h3>
  <p>Illustration Guidelines help all designers reach the same destination when designing for Sage.com. They help to keep conventions consistent, whether designing for print or the web.</p>
  <p>This guide is available for designers internally and externally, and should be continually referred to.</p>
  <p>Illustration is a great way to communicate and tell a story. It gives a personable touch and makes the overall design experience feel authentic. Sage has adopted a unique style which is distinctive, hand-drawn and colourful.</p>
  <p>Our aims with using illustration are to help us stand out amongst our competitors and inject some individuality into the brand. We use the versatility of illustration to simplify and explain complex concepts and express compelling stories.</p>
  <div class='dls-do-dont-wrapper'>
    <div class='dls-neutral'>
      <div class='dls-img large'>
        <img src='/visuals/illustration-examples/guidance/summary.png' id='spot-1'>
      </div>
    </div>
  </div>
  <div class='container'>
    <h2>Brushes</h2>
    <p>These are the brushes used to achieve our style. Please install the <a href='/downloads/illustration/Sage Vector Brush Set.ai' target='_blank'>Sage Vector Brush Set</a>.</p>
    <div class='dls-do-dont-wrapper'>
      <div class='dls-do'>
        <div class='dls-img'>
          <img src='/visuals/illustration-examples/guidance/brushes-do.png' id='brushes-do'>
        </div>
        <figcaption id='brushes-do'>
          <p><span class='span-do'>Do</span> add pencil brushes to the strokes of the vectors for a hand-drawn look and feel.  Carefully select brushes - our preference is <span class='emphasis-term'>PNCL-HARD15</span>.</p>
        </figcaption>
      </div>
      <div class='dls-dont'>
        <div class='dls-img'>
          <img src='/visuals/illustration-examples/guidance/brushes-dont.png' id='brushes-dont'>
        </div>
        <figcaption id='brushes-dont'>
          <p><span class='span-do'>Don’t</span> use any other brush outside the <a href='/downloads/illustration/Sage Vector Brush Set.ai' target='_blank'>Sage Vector Brush Set</a>. Avoid sharp edges for a hand-drawn look and feel.</p>
        </figcaption>
      </div>
    </div>
  </div>
  <div class='container'>
    <h3>Strokes</h3>
    <div class='dls-do-dont-wrapper'>
      <div class='dls-do'>
        <div class='dls-img'>
          <img src='/visuals/illustration-examples/guidance/strokes-do.png' id='strokes-do'>
        </div>
        <figcaption id='strokes-do'>
          <p><span class='span-do'>Do</span> use strokes on all elements of the illustration e.g. the jigsaw pieces, hands, finger nails, etc. </p><p><span class='span-do'>Do</span> use line art to define creases and features, using varied stroke widths.</p>
        </figcaption>
      </div>
      <div class='dls-dont'>
        <div class='dls-img'>
          <img src='/visuals/illustration-examples/guidance/strokes-dont.png' id='strokes-dont'>
        </div>
        <figcaption id='strokes-dont'>
          <p><span class='span-dont'>Don’t</span> use the slate stroke for the whole character, object, etc. </p><p><span class='span-dont'>Don’t</span> use the same stroke size throughout.</p>
        </figcaption>
      </div>
    </div>
  </div>
  <div class='container'>
    <h2>Characters</h2>
    <p>The Sage illustrative world is not bound by reality, making it a great way to convey abstract concepts that cannot be conveyed with photography.</p>
  </div>
  <div class='container'>
    <h3>Process</h3>
    <p>There are 4 steps to create a Sage character illustration from start to finish.  If illustrations need to be scaled up or down, copy and paste the AI document and use <span class='emphasis-term'>Object > Expand Appearance</span> to retain the stroke pixel size. </p>
    <div class='dls-do-dont-wrapper'>
      <div class='dls-neutral'>
        <div class='dls-img'>
          <img src='/visuals/illustration-examples/guidance/character-1.png' id='character-1'>
        </div>
        <figcaption id='character-1'>
          <p>1. Base your illustration on a reference from the <a href='https://sage-ia.workfrontdam.com/bp/#/folder/4665987/' target='_blank'>Sage Digital Library</a>, an existing character illustration, or even someone a team member.</p>
        </figcaption>
      </div>
      <div class='dls-neutral'>
        <div class='dls-img'>
          <img src='/visuals/illustration-examples/guidance/character-2.png' id='character-2'>
        </div>
        <figcaption id='character-2'>
          <p>2. Sketch out the character and exaggerate angles - we want illustrations to have a geometric aesthetic.</p>
        </figcaption>
      </div>
      <div class='dls-neutral'>
        <div class='dls-img'>
          <img src='/visuals/illustration-examples/guidance/character-3.png' id='character-3'>
        </div>
        <figcaption id='character-3'>
          <p>3. Scan and use the sketch in Adobe Illustrator to vectorise. Place the scan on the top layer and use the multiply blending option.</p>
        </figcaption>
      </div>
      <div class='dls-neutral'>
        <div class='dls-img'>
          <img src='/visuals/illustration-examples/guidance/character-4.png' id='character-4'>
        </div>
        <figcaption id='character-4'>
          <p>4. Add features and shadows using the vector brushes. Don't outline live strokes in the final document.</p>
        </figcaption>
      </div>
    </div>
  </div>
  <div class='container'>
    <h3>General Character Usage</h3>
    <p>Illustration should be used in instances where photography can't depict what you need to say. </p>
    <p>Illustrations can be used to make diagrams seem more visually dynamic, or for print collateral. All files are set up as RGB / 300 DPI, but when sending illustrations to print, make a copy in CMYK. Always keep the original in RGB.</p>
    <div class='dls-do-dont-wrapper'>
      <div class='dls-neutral'>
        <div class='dls-img large'>
          <img src='/visuals/illustration-examples/guidance/general-character.png'>
        </div>
      </div>
    </div>
  </div>
  <div class='container'>
    <h4>Best Practices</h4>
    <div class='dls-do-dont-wrapper'>
      <div class='dls-do small'>
        <div class='dls-img small'>
          <img src='/visuals/illustration-examples/guidance/character-do-1.png' id='character-do-1'>
        </div>
        <figcaption id='character-do-1'>
          <p><span class='span-do'>Do</span> include diversity in your characters. Sage has a global reach of 23 countries, so it’s important to be inclusive.</p>
        </figcaption>
      </div>
      <div class='dls-do'>
        <div class='dls-img small'>
          <img src='/visuals/illustration-examples/guidance/character-do-2.png' id='character-do-2'>
        </div>
        <figcaption id='character-do-2'>
          <p><span class='span-do'>Do</span> use the black brush for outlines on darker skin and  the slate colour on lighter  skin tones.</p>
        </figcaption>
      </div>
      <div class='dls-do'>
        <div class='dls-img small'>
          <img src='/visuals/illustration-examples/guidance/character-do-3.png' id='character-do-3'>
        </div>
        <figcaption id='character-do-3'>
          <p><span class='span-do'>Do</span> use shades darker than the hue when creating shadows and depth. Refer to the colour guide.</p>
        </figcaption>
      </div>
      <div class='dls-dont'>
        <div class='dls-img small'>
          <img src='/visuals/illustration-examples/guidance/character-dont-1.png' id='character-dont-1'>
        </div>
        <figcaption id='character-dont-1'>
          <p><span class='span-dont'>Don't</span> make unrealistic colour choices - base your characters on real people.</p>
        </figcaption>
      </div>
      <div class='dls-dont'>
        <div class='dls-img small'>
          <img src='/visuals/illustration-examples/guidance/character-dont-2.png' id='character-dont-2'>
        </div>
        <figcaption id='character-dont-2'>
          <p><span class='span-dont'>Don't</span> overcomplicate with unnecessary details, such as complex patterns on clothing.</p>
        </figcaption>
      </div>
      <div class='dls-dont'>
        <div class='dls-img small'>
          <img src='/visuals/illustration-examples/guidance/character-dont-3.png' id='character-dont-3'>
        </div>
        <figcaption id='character-dont-3'>
          <p><span class='span-dont'>Don't</span> use illustrations for purposes other than they were originally intended - e.g. 'No Connection' shouldn't also be used for 'No search results'.</p>
        </figcaption>
      </div>
    </div>
  </div>
  <div class='container'>
    <h3>How to Crop Illustrations</h3>
    <p>When you don't need a full sized character, follow this guidance to create a character avatar.</p>
    <div class='dls-do-dont-wrapper'>
      <div class='dls-do'>
        <div class='dls-img small'>
          <img src='/visuals/illustration-examples/guidance/crop-1.png' id='crop-1'>
        </div>
        <figcaption id='crop-1'>
          <p><span class='span-do'>Do</span> crop from the  waist upwards.</p>
        </figcaption>
      </div>
      <div class='dls-do'>
        <div class='dls-img small'>
          <img src='/visuals/illustration-examples/guidance/crop-2.png' id='crop-2'>
        </div>
        <figcaption id='crop-2'>
          <p><span class='span-do'>Do</span> crop to focus on the face, giving enough space.</p>
        </figcaption>
      </div>
      <div class='dls-do'>
        <div class='dls-img small'>
          <img src='/visuals/illustration-examples/guidance/crop-3.png' id='crop-3'>
        </div>
        <figcaption id='crop-3'>
          <p><span class='span-do'>Do</span> crop outside the shape container.</p>
        </figcaption>
      </div>
      <div class='dls-dont'>
        <div class='dls-img small'>
          <img src='/visuals/illustration-examples/guidance/crop-4.png' id='crop-4'>
        </div>
        <figcaption id='crop-4'>
          <p><span class='span-dont'>Don't</span> scale down so the features aren't clear.</p>
        </figcaption>
      </div>
      <div class='dls-dont'>
        <div class='dls-img small'>
          <img src='/visuals/illustration-examples/guidance/crop-5.png' id='crop-5'>
        </div>
        <figcaption id='crop-5'>
          <p><span class='span-dont'>Don't</span> use background colours that blend into the character.</p>
        </figcaption>
      </div>
      <div class='dls-dont'>
        <div class='dls-img small'>
          <img src='/visuals/illustration-examples/guidance/crop-6.png' id='crop-6'>
        </div>
        <figcaption id='crop-6'>
          <p><span class='span-dont'>Don't</span> crop off limbs. Be mindful of framing.</p>
        </figcaption>
      </div>
    </div>
  </div>
  <div class='container'>
    <h3>How to Scale Illustrations</h3>
    <p>If the features of a character aren't clear, the illustration is too small. Try not to go any smaller than 50 x 150 pixels.</p>
    <div class='dls-do-dont-wrapper'>
      <div class='dls-neutral'>
        <div class='dls-img large'>
          <img src='/visuals/illustration-examples/guidance/scale.png'>
        </div>
      </div>
    </div>
  </div>
  <div class='container'>
    <h3>Composition Guidelines</h3>
    <p>The best way to get an idea of our composition style is to study our existing illustrations. Follow general graphic design principles.</p>
    <div class='dls-do-dont-wrapper'>
      <div class='dls-do'>
        <div class='dls-img large'>
          <img src='/visuals/illustration-examples/guidance/composition-1.png' id='composition-1'>
        </div>
        <figcaption class='caption-left' id='composition-1'>
          <p><span class='span-do'>Do</span> create compositions so you are able to view characters’ expressions and identify objects without having to zoom in.</p>
        </figcaption>
        <figcaption class='caption-right' id='composition-1'>
          <p><span class='span-do'>Do</span> be mindful of scale if using a collage of characters in the same environment, e.g. characters standing alongside one another.</p>
        </figcaption>
      </div>
    </div>
    <div class='dls-do-dont-wrapper'>
      <div class='dls-dont'>
        <div class='dls-img large'>
          <img src='/visuals/illustration-examples/guidance/composition-2.png' id='composition-2'>
        </div>
        <figcaption class='caption-left' id='composition-2'>
          <p><span class='span-dont'>Don't</span> make objects or characters too small. Nearly 1 in 2 people view Sage apps on their mobile devices.</p>
        </figcaption>
        <figcaption class='caption-right' id='composition-2'>
          <p><span class='span-dont'>Don't</span> overcrowd illustrations - balance characters and objects within the scene.</p>
        </figcaption>
      </div>
    </div>
  </div>
  <div class='container'>
    <h2>Background</h2>
    <p>Depending on where the illustration(s) will live, you'll need to take the background palette into consideration. Characters and assets important to the scene need to be the main focus. Reducing the prominence of scene assets that aren’t important helps to make this clear.</p>
  </div>
  <div class='container'>
    <div class='dls-do-dont-wrapper'>
      <div class='dls-neutral'>
        <div class='dls-img large'>
          <img src='/visuals/illustration-examples/guidance/background-1.png' id='background-1'>
        </div>
        <figcaption class='caption-fullwidth' id='background-1'>
          <p>In lighter background scenes, use the lightest palette form of Slate. <br/>The relevant elements within the environment use colour to make the scene pop.</p>
        </figcaption>
      </div>
    </div>
    <div class='dls-do-dont-wrapper'>
      <div class='dls-neutral'>
        <div class='dls-img large'>
          <img src='/visuals/illustration-examples/guidance/background-2.png' id='background-2'>
        </div>
        <figcaption class='caption-fullwidth' id='background-2'>
          <p>In darker background scenes, use a darker palette form of Slate. <br/>The relevant elements within the environment use colour to make the scene pop.</p>
        </figcaption>
      </div>
    </div>
  </div>
  <div class='container'>
    <h2>Objects</h2>
    <p>Character illustration is more involved and time consuming than illustrating objects, as work needs to be put into capturing personality. However, a character isn't always necessary. It's up to you, the llustrator, to decide if the graphic needed is instructional, or focusses on something specific.</p>
  </div>
  <div class='container'>
    <h3>Shapes / Icons</h3>
    <div class='dls-do-dont-wrapper'>
      <div class='dls-do'>
        <div class='dls-img'>
          <img src='/visuals/illustration-examples/guidance/shapes-do.png' id='shapes-do'>
        </div>
        <figcaption id='shapes-do'>
          <p><span class='span-do'>Do</span> use solid fill and/or shading to define objects. <br/><span class='span-do'>Do</span> use geometric shapes where possible. <br/><span class='span-do'>Do</span> simplify shapes as much as possible.</p>
        </figcaption>
      </div>
      <div class='dls-dont'>
        <div class='dls-img'>
          <img src='/visuals/illustration-examples/guidance/shapes-dont.png' id='shapes-dont'>
        </div>
        <figcaption id='shapes-dont'>
          <p><span class='span-dont'>Don't</span> use the full Sage gradient within objects. <br/><span class='span-dont'>Don't</span> overcomplicate with unnecessary details. <br/><span class='span-dont'>Don't</span> use radical gradients.</p>
        </figcaption>
      </div>
    </div>
  </div>
  <div class='container'>
    <h3>Text in Illustration</h3>
    <div class='dls-do-dont-wrapper'>
      <div class='dls-do'>
        <div class='dls-img'>
          <img src='/visuals/illustration-examples/guidance/text-do.png' id='text-do'>
        </div>
        <figcaption id='text-do'>
          <p><span class='span-do'>Do</span> use illustration or icons to represent  words.</p>
        </figcaption>
      </div>
      <div class='dls-dont'>
        <div class='dls-img'>
          <img src='/visuals/illustration-examples/guidance/text-dont.png' id='text-dont'>
        </div>
        <figcaption id='text-dont'>
          <p><span class='span-dont'>Don't</span> use words or currency symbols in illustrations that are going to be seen in more than one locale.</p>
        </figcaption>
      </div>
    </div>
  </div>
  <div class='container'>
    <h2>Spot Illustration and Icons</h2>
    <p>Spot Illustration and Icons are used to highlight featured areas on pages and add  personality.</p>
  </div>
  <div class='container'>
    <div class='dls-do-dont-wrapper'>
      <div class='dls-neutral'>
        <div class='dls-img'>
          <img src='/visuals/illustration-examples/guidance/spot-1.png' id='spot-1'>
        </div>
        <figcaption id='spot-1'>
          <p>  1. Start with a simple UI icon for smaller sizes.</p>
        </figcaption>
      </div>
      <div class='dls-neutral'>
        <div class='dls-img'>
          <img src='/visuals/illustration-examples/guidance/spot-2.png' id='spot-2'>
        </div>
        <figcaption id='spot-2'>
          <p>  2. Tablet or mobile icons can be larger and more intricate.</p>
        </figcaption>
      </div>
      <div class='dls-neutral'>
        <div class='dls-img'>
          <img src='/visuals/illustration-examples/guidance/spot-3.png' id='spot-3'>
        </div>
        <figcaption id='spot-3'>
          <p>  3. Spot illustrations have a consistent 'editorial' style, and limited in palette, with flashes of colour.</p>
        </figcaption>
      </div>
      <div class='dls-neutral'>
        <div class='dls-img'>
          <img src='/visuals/illustration-examples/guidance/spot-4.png' id='spot-4'>
        </div>
        <figcaption id='spot-4'>
          <p>4. Empty state illustrations use colour to make the graphic obvious to the user. In the 'Connect Your Bank' example we use grey as a way of conveying when something is absent.</p>
        </figcaption>
      </div>
    </div>
  </div>
  <div class='container'>
    <h3>Exception</h3>
    <p>Sage Business Cloud has more than 3,000 possible states. In situations like this, it's best to use generic illustrations or icons to represent a group of states, rather than a personalised family like this example.</p>
  </div>
  <div class='container'>
    <h2>Technology</h2>
    <p>Illustration of devices, screens, and tech concepts.</p>
  </div>
  <div class='container'>
    <div class='dls-do-dont-wrapper'>
      <div class='dls-neutral'>
        <div class='dls-img'>
          <img src='/visuals/illustration-examples/guidance/tech-1.png' id='tech-1'>
        </div>
      </div>
      <div class='dls-neutral'>
        <div class='dls-img'>
          <img src='/visuals/illustration-examples/guidance/tech-2.png' id='tech-2'>
        </div>
      </div>
    </div>
    <p>If a device screen is part of the illustration scene, we take a simplistic, abstract approach and use the colour palette green.</p>
  </div>
  <div class='container'>
    <div class='dls-do-dont-wrapper'>
      <div class='dls-neutral'>
        <div class='dls-img'>
          <img src='/visuals/illustration-examples/guidance/tech-3.png' id='tech-3'>
        </div>
      </div>
      <div class='dls-neutral'>
        <div class='dls-img'>
          <img src='/visuals/illustration-examples/guidance/tech-4.png' id='tech-4'>
        </div>
      </div>
    </div>
    <p>If the screen contains a screenshot of real user interface, we express a more serious and realistic form using the colour palette slate.</p>
  </div>
  <div class='container'>
    <h2>Motion</h2>
    <p>Apply motion and animation thoughtfully and with purpose. Motion should be meaningful and functional,  enhancing the user experience. <a href='https://www.dropbox.com/sh/zwrj3kh9ckv3mj6/AABArkHLnMwaDqWMdn7wgr1Ya?dl=0' target='_blank'>View Examples</a></p>
  </div>
  <div class='container'>
    <h3>Steps</h3>
    <ol class='ordered-list'>
      <li>Before animating, it's best to have a storyboard in place, so it's clear if characters  need to have more than one profile.</li>
      <li>Duplicate the Adobe Illustrator file so you can edit each component to be animated.  Live strokes will need to be expanded so they're part of the illustration whole.</li>
      <li>Name each layer so it’s obvious what will be animated - this makes life easier when  exporting into After Effects.</li>
      <li>Export as an MP4.</li>
    </ol>
    <div class='dls-do-dont-wrapper'>
      <div class='dls-neutral'>
        <div class='dls-img large'>
          <img src='/visuals/illustration-examples/guidance/motion-1.png'>
          <img src='/visuals/illustration-examples/guidance/motion-2.png'>
          <img src='/visuals/illustration-examples/guidance/motion-3.png'>
        </div>
      </div>
    </div>
  </div>
  <div class='container'>
    <h2>Best Practice</h2>
    <h3>Illustration location</h3>
    <p>Dropbox (sage.com XD Team) > Illustration > 2 WORK <br/>
      They are separated into groups, e.g. the No Connection Illustration lives here:  Dropbox (sage.com XD Team > Illustration > 2 WORK > 2 Messaging Icons and Illustrations > 1 No Connection</p>
      <h3>Folder structure</h3>
      <p>0 Archive , 1 Inspiration and Sketches , 2 Illustrator , 3 FINAL</p>
      <h3>Naming Convention</h3>
      <p>graphic type_group_specific to the group   e.g. No Connection: 'illustration_messaging_no_connection.png'</p>
      <h3>Exporting Options</h3>
      <p>Illustrations are best exported as PNGs, with a file size that is as small as possible, not to  exceed 4000px  in width.</p>
      <p>Optimise the image with a service such as <a href='http://tinypng.com' target='_blank'>tinypng.com</a></p>
      <h3>Illustration Requests</h3>
      <p>Please don't tweak any illustrations. If none that already exist translate to what is needed, please ask Mike Puxley or Ben Wilson so Michelle Hird can create them.</p>
    </div>
  </div>
  <div class='secondary-tab hidden' id='tabs-2'>

    <div class="container container-flex flex-2">
      <p>To see the new illustration style come to life, download this <a href='https://sage365-my.sharepoint.com/:u:/g/personal/ben_wilson_sage_com/EZo5gUmyyVRLuTqfa5_8R3sBSOfzhbctsLbqkMBMlUB4sA?e=78drzf' target='_blank'>animated Keynote</a>.</p>

      <div class="pod">
        <h2>Characters</h2>
        <p>Represent our customers themselves.</p>
        <div class="download-file">
          <a href="/foundations/illustration/characters/" class="dls-button size-small business-generic secondary">View</a>
        </div>
      </div>
      <div class="pod">
        <h2>Customer Segments</h2>
        <p>Represent a group of people - for example, developers, or large business customers.</p>
        <div class="download-file">
          <a href="/foundations/illustration/segments/" class="dls-button size-small business-generic secondary">View</a>
        </div>
      </div>
      <div class="pod">
        <h2>Messaging</h2>
        <p>Deliver a specific message to the user in context - for example, 404, no search results.</p>
        <div class="download-file">
          <a href="/foundations/illustration/messaging/" class="dls-button size-small business-generic secondary">View</a>
        </div>
      </div>
      <div class="pod">
        <h2>Conceptual Benefits</h2>
        <p>For example, collaboration, compliance, simplicity.</p>
        <div class="download-file">
          <a href="/foundations/illustration/benefits/" class="dls-button size-small business-generic secondary">View</a>
        </div>
      </div>
      <div class="pod">
        <h2>Device Illustrations</h2>
        <p>Desktop, laptop, tablet and mobile device illustrations to frame your UI designs.</p>
        <div class="download-file">
          <a href="/foundations/illustration/devices/" class="dls-button size-small business-generic secondary">View</a>
        </div>
      </div>
      <div class="pod">
        <h2>First Time Use</h2>
        <p>Developed specially for a <a href='/patterns/ftu/'>first time use</a> context.</p>
        <div class="download-file">
          <a href="/foundations/illustration/ftu/" class="dls-button size-small business-generic secondary">View</a>
        </div>
      </div>
    </div>
  </div>
  <div class='secondary-tab hidden' id='tabs-3'>
    <h2>Primary Colour Palette</h2>
    <p>Main palette to use for clothing, outlining and technology elements. The Sage Rainbow is used for subtle graphics.</p>
    <div class="container container-flex flex-3">
      {% for colour in site.data.illustration_colours %}
      <div class="illustration-spectrum">
        <div class="illustration-colour copy-to-clipboard" data-clipboard-text="{{ colour.hex_code }}" style="background-color: {{ colour.hex_code }};">
          <div class="illustration-caption">
            <p><span class='emphasis-term'>Name </span>{{ colour.name }}</p>
            <p><span class='emphasis-term'>Hex </span>{{ colour.hex_code }}</p>
          </div>
        </div>
      </div>
      {% endfor %}
    </div>
    <h2>Extended Colour Palette</h2>
    <p>Mostly used for: shading, creating contrast between similar colours, or to vary characters' outfits.</p>
    <div class="container container-flex flex-3">
      {% for entry in site.data.illustration_extended %}
      <div class="illustration-extended">
        {% for levels in entry.levels %}
        <div class="illustration-extended-colour copy-to-clipboard" data-clipboard-text="{{ levels.level }}" style="background-color: {{ levels.level }};">
          <p style='color: {{ levels.textcolor }}'><span class='emphasis-term'>Hex </span>{{ levels.level }}</p>
        </div>
        {% endfor %}
      </div>
      {% endfor %}
    </div>
    <h2>Skin Colour Palette</h2>
    <p>Main palette for skin colour.</p>
    <div class="container container-flex flex-3">
      {% for colour in site.data.illustration_skin %}
      <div class="illustration-spectrum">
        <div class="illustration-colour copy-to-clipboard" data-clipboard-text="{{ colour.hex_code }}" style="background-color: {{ colour.hex_code }};">
          <div class="illustration-caption">
            <p><span class='emphasis-term'>Name </span>{{ colour.name }}</p>
            <p><span class='emphasis-term'>Hex </span>{{ colour.hex_code }}</p>
          </div>
        </div>
      </div>
      {% endfor %}
    </div>
    <h2>Extended Skin Colour Palette</h2>
    <p>Mostly used for shading skin areas.</p>
    <div class="container container-flex flex-3">
      {% for entry in site.data.illustration_extended_skin %}
      <div class="illustration-extended">
        {% for levels in entry.levels %}
        <div class="illustration-extended-colour copy-to-clipboard" data-clipboard-text="{{ levels.level }}" style="background-color: {{ levels.level }};">
          <p style='color: {{ levels.textcolor }}'><span class='emphasis-term'>Hex </span>{{ levels.level }}</p>
        </div>
        {% endfor %}
      </div>
      {% endfor %}
    </div>
    <h2>Background Colour Palette</h2>
    <p>The muted palette below has been created for background illustrations and empty state illustrations/icons.</p>
    <div class="container container-flex flex-3">
      {% for entry in site.data.illustration_extended_muted %}
      <div class="illustration-extended">
        {% for levels in entry.levels %}
        <div class="illustration-extended-colour copy-to-clipboard" data-clipboard-text="{{ levels.level }}" style="background-color: {{ levels.level }};">
          <p style='color: {{ levels.textcolor }}'><span class='emphasis-term'>Hex </span>{{ levels.level }}</p>
        </div>
        {% endfor %}
      </div>
      {% endfor %}
    </div>
  </div>
