---
layout: default
secondary_menu_id: accessibility
primary_menu_id: foundations
title: Accessibility
summary: How we build Sage Business Cloud to work for everyone.
tags: accessibility, wcag, contrast, standards, standard
---

<p class='summary'>{{ page.summary }}</p>
<div class='container'>
  <div class="system-message info-message ">
    <div class='system-message-title'>WCAG Version 2.1 Update</div>
    <div class='system-message-body'>This summary includes the new and updated requirements released by WCAG in June 2018.</div>
  </div>
  <p>Everyone should be able to benefit from Sage Business Cloud. We're committed to building experiences that are as accessible to all as we can make them.</p>
  <ul class="standard-list">
    <li>We aim to comply with the <a href='https://www.w3.org/TR/WCAG21/' target='_blank'>Web Content Accessibility Guidelines (WCAG) 2.1</a> (June 2018) to at least AA level, because:
      <ul class="standard-list">
        <li>AA level matches public sector accessibility requirements in <a href='https://www.gov.uk/guidance/accessibility-requirements-for-public-sector-websites-and-apps' target='_blank'>the UK</a> and <a href='http://mandate376.standards.eu/standard/technical-requirements/#9' target='_blank'>Europe</a>.</li>
        <li>Technical requirements of <a href='https://www.access-board.gov/guidelines-and-standards/communications-and-it/about-the-ict-refresh/background/comparison-table-of-wcag2-to-existing-508-standards' target='_blank'>Section 508 in the US</a> now incorporate WCAG 2.0 A and AA success criteria.</li>
      </ul>
    </li>
    <li>For practical guidance, we refer to WCAG's Quick Reference success criteria and guidance notes at <a href='https://www.w3.org/WAI/WCAG21/quickref/' target='_blank'>www.w3.org</a>.</li>
  </ul>
  <p>This summary isn't exhaustive - for example, it doesn't include purely technical requirements. It suggests practical actions to take to comply with the standard, rather than just listing the requirements of the standard itself. For more details, browse our <a href='https://docs.google.com/spreadsheets/d/13YFNl92mlH-SfmVe1rEs9OoHqVLOTN2q6mX9NJRYVnA/edit?usp=sharing' target='_blank'>matrix of requirements</a>.</p>
</div>
<div class='container'>
  <h2><a id='accessibility-responsive'></a>When considering page size, layout, and orientation...</h2>
  <ul class="standard-list">
    <li>Design the page to work when zoomed to 200% (it's <a href='https://www.w3.org/TR/WCAG20-TECHS/G142.html' target='_blank'>OK to rely on the web browser</a> to do this, but check your pages display well) [<a href='https://www.w3.org/WAI/WCAG21/Understanding/resize-text.html' target='_blank'>1.4.4</a>].
      <ul class="standard-list">
        <li>It's usually better for the browser to scale the entire page, rather than just the text elements, to preserve a good layout [<a href='https://www.w3.org/WAI/WCAG21/Understanding/resize-text.html' target='_blank'>1.4.4</a>].</li>
        <li>It's only OK to truncate content as a result of scaling if there is a means of viewing the full, untruncated content [<a href='https://www.w3.org/WAI/WCAG21/Understanding/resize-text.html' target='_blank'>1.4.4</a>].</li>
      </ul>
    </li>
    <li><div class='dls-pill-container dls-badge badge-generic dls-badge-use'><span class="dls-pill">New</span></div>Design your pages to work in both portrait and landscape orientation [<a href='https://www.w3.org/WAI/WCAG21/Understanding/orientation.html' target='_blank'>1.3.4</a>].</li>
    <li><div class='dls-pill-container dls-badge badge-generic dls-badge-use'><span class="dls-pill">New</span></div>Design your pages to be responsive, so horizontal scrolling isn't needed at 320px page width [<a href='https://www.w3.org/WAI/WCAG21/Understanding/reflow.html' target='_blank'>1.4.10</a>].
      <ul class="standard-list">
        <li>Content should 'reflow' (organise itself into a single column), so that scrolling in more than one direction isn't necessary.</li>
        <li>To test this, WCAG recommend setting the browser window to 1280 pixels wide and then zooming the page content to 400%.</li>
        <li>WCAG suggest that web browsers on mobiles may be exempt, but this requirement applies to desktop browsers with small viewports too.</li>
        <li>If scrolling is 'unavoidable', for example, with a large table of data, then WCAG suggest this requirement can be 'exempted'.</li>
        <li>Check out the <a href='/foundations/grid/'>grid system</a> for more on how to approach this.</li>
      </ul>
    </li>
    <li>Avoid multi-column layouts [<a href='https://www.w3.org/WAI/WCAG21/Understanding/meaningful-sequence.html' target='_blank'>1.3.2</a>].</li>
    <li>Avoid layouts with more than one content area [<a href='https://www.w3.org/WAI/WCAG21/Understanding/meaningful-sequence.html' target='_blank'>1.3.2</a>].</li>
  </ul>
</div>
<div class='container'>
  <h2><a id='accessibility-mobile'></a>When designing for mobile devices...</h2>
  <ul class="standard-list">
    <li><div class='dls-pill-container dls-badge badge-generic dls-badge-use'><span class="dls-pill">New</span></div> If you design a feature that's triggered by moving the device (e.g. shaking or panning) or by user movement (e.g. waving to a camera), make sure this can be disabled, and there's another way to do the same thing [<a href='https://www.w3.org/WAI/WCAG21/Understanding/motion-actuation.html' target='_blank'>2.5.4</a>].</li>
    <li><div class='dls-pill-container dls-badge badge-generic dls-badge-use'><span class="dls-pill">New</span></div> If you design a feature that relies on complex gestures (such as pinching, swiping, or dragging), make sure there's also a way to do the same thing with a simple gesture (e.g. tapping) [<a href='https://www.w3.org/WAI/WCAG21/Understanding/pointer-gestures.html' target='_blank'>2.5.1</a>].</li>
    <li>Check out the <a href='/mobile/'>mobile section</a> for more on how to approach this.</li>
  </ul>
</div>
<div class='container'>
  <h2><a id='accessibility-contrast'></a>When designing colour, contrast, and visuals</h2>
  <h3>General</h3>
  <ul class="standard-list">
    <li>Design System components are compliant with minimum contrast standards, but it's always worth checking contrast ratios in your own context. <a href='https://webaim.org/resources/contrastchecker/' target='_blank'>Webaim have a handy tool</a>, and there's a <a href='http://sketchapp.rocks/plugins/color-contrast-analyser/' target='_blank'>Sketch plugin</a> too.</li>
    <li>Don't use colour alone as the only way to distinguish an element or its state - change the object's form too. Be particularly careful with charts and graphs - using colour alone in a chart's legend is a common pitfall [<a href='https://www.w3.org/WAI/WCAG21/Understanding/use-of-color.html' target='_blank'>1.4.1</a>].</li>
  </ul>
  <h3>Text Contrast</h3>
  <ul class="standard-list">
    <li>Body text, text in buttons, and links should have a contrast ratio of at least 4.5:1 against their background (unless the text size is over 18 point, or 14 point and bold, where a different ratio applies). [<a href='https://www.w3.org/WAI/WCAG21/Understanding/contrast-minimum.html' target='_blank'>1.4.3</a>].</li>
    <li>There is no text contrast requirement for inactive elements. [<a href='https://www.w3.org/WAI/WCAG21/Understanding/contrast-minimum.html' target='_blank'>1.4.3</a>].</li>
  </ul>
  <h3>Non-Text Contrast</h3>
  <ul class="standard-list">
    <li>There are no contrast requirements for buttons, other than the contrast of the text within them as above [<a href='https://www.w3.org/WAI/WCAG21/Understanding/non-text-contrast.html' target='_blank'>1.4.11</a>].</li>
    <li>Because our link style is underlined, it <a href='https://www.w3.org/TR/WCAG20-TECHS/F73.html' target='_blank'>doesn't specifically need to have a contrast of 3:1</a> compared to a non-link text. But we've chosen colours that meet this contrast ratio anyway for good measure.</li>
    <li><div class='dls-pill-container dls-badge badge-generic dls-badge-use'><span class="dls-pill">New</span></div>Icons should have a contrast ratio of at least 3:1</a> against their background [<a href='https://www.w3.org/WAI/WCAG21/Understanding/non-text-contrast.html' target='_blank'>1.4.11</a>].</li>
    <li><div class='dls-pill-container dls-badge badge-generic dls-badge-use'><span class="dls-pill">New</span></div>Input field borders should have a contrast ratio of at least 3:1 against their background [<a href='https://www.w3.org/WAI/WCAG21/Understanding/non-text-contrast.html' target='_blank'>1.4.11</a>].</li>
    <li><div class='dls-pill-container dls-badge badge-generic dls-badge-use'><span class="dls-pill">New</span></div>Disabled or read-only elements (buttons, inputs, links) don't have any contrast ratio requirements [<a href='https://www.w3.org/WAI/WCAG21/Understanding/non-text-contrast.html' target='_blank'>1.4.11</a>].</li>
    <li>Because we change the form of buttons, links and inputs on focus (by adding an outline), they don't need to have a contrast of 3:1 compared to a non-focus state or the background [<a href='https://www.w3.org/WAI/WCAG21/Understanding/non-text-contrast.html' target='_blank'>1.4.11</a>].</li>
  </ul>
  <h3>Motion and Animation</h3>
  <ul class="standard-list">
    <li>Avoid moving, blinking, flashing, or scrolling content, and don’t automatically cycle through carousels [<a href='https://www.w3.org/WAI/WCAG21/Understanding/pause-stop-hide.html' target='_blank'>2.2.2</a>].</li>
    <li>Don't design any animation or loading effect that results in a blinking or flashing. Check that the use of dynamic technologies doesn't accidentaly result in flashing too [<a href='https://www.w3.org/WAI/WCAG21/Understanding/three-flashes-or-below-threshold.html' target='_blank'>2.3.1</a>].</li>
    <li>If interacting with something triggers an animation, then offer a way to disable this. Animations that last less than 5 seconds are OK (e.g. typical loading spinners) [<a href='https://www.w3.org/WAI/WCAG21/Understanding/pause-stop-hide.html' target='_blank'>2.2.2</a>].
    </li>
  </ul>
</div>
<div class='container'>
  <h2><a id='accessibility-links'></a>When designing navigation and links...</h2>
  <h3>Navigation</h3>
  <ul class="standard-list">
    <li>Place navigation in the same place on all pages [<a href='https://www.w3.org/WAI/WCAG21/Understanding/consistent-navigation.html' target='_blank'>3.2.3</a>].</li>
    <li>Place 'skip navigation' links in the same place on all pages [<a href='https://www.w3.org/WAI/WCAG21/Understanding/consistent-navigation.html' target='_blank'>3.2.3</a>].</li>
    <li>Place navigation items in the same relative order on all pages [<a href='https://www.w3.org/WAI/WCAG21/Understanding/consistent-navigation.html' target='_blank'>3.2.3</a>].</li>
    <li>Consider a <a href='/components/global-search-bar' target='_blank'>global search mechanism</a> or other secondary means of navigation [<a href='https://www.w3.org/WAI/WCAG21/Understanding/multiple-ways.html' target='_blank'>2.4.5</a>].</li>
    <li>Check out the <a href='/components/navigation/'>navigation component</a> for more on how to approach this.</li>
  </ul>
  <h3>Links</h3>
  <ul class="standard-list">
    <li>Give links clear names so a user can understand their purpose from the text alone (avoid things like ‘Click here’) [<a href='https://www.w3.org/WAI/WCAG21/Understanding/link-purpose-in-context.html' target='_blank'>2.4.4</a>].</li>
    <li>If links go to different destinations, name the links differently. If they go to the same destination, give them the same name [<a href='https://www.w3.org/WAI/WCAG21/Understanding/link-purpose-in-context.html' target='_blank'>2.4.4</a>].</li>
    <li>Make sure navigation items or links don’t change order on different pages [<a href='https://www.w3.org/WAI/WCAG21/Understanding/link-purpose-in-context.html' target='_blank'>2.4.4</a>].</li>
    <li>If you need to provide additional context, associate this text with the link in code [<a href='https://www.w3.org/WAI/WCAG21/Understanding/link-purpose-in-context.html' target='_blank'>2.4.4</a>].</li>
    <li>If a link has no overt text, because it takes the form of an icon, then provide descriptive text in code [<a href='https://www.w3.org/WAI/WCAG21/Understanding/link-purpose-in-context.html' target='_blank'>2.4.4</a>].</li>
    <li>Check out the <a href='/components/links/'>links component</a> for more on how to approach this.</li>
  </ul>
</div>
<div class='container'>
  <h2><a id='accessibility-forms'></a>When designing text elements...</h2>
  <ul class="standard-list">
    <li>Don’t use images to display pictures of text - always use text elements [<a href='https://www.w3.org/WAI/WCAG21/Understanding/images-of-text.html' target='_blank'>1.4.5</a>].</li>
    <li>Design informative page headings and labels for forms. Don’t use the same heading in more than one place on the page (avoid things like ‘More Details’) [<a href='https://www.w3.org/WAI/WCAG21/Understanding/info-and-relationships.html' target='_blank'>1.3.1</a>].</li>
    <li>Design your pages to have <a href='/foundations/text-font/'>semantic headings</a> (h1, h2, h3...) [<a href='https://www.w3.org/WAI/WCAG21/Understanding/info-and-relationships.html' target='_blank'>1.3.1</a>].</li>
    <li><div class='dls-pill-container dls-badge badge-generic dls-badge-use'><span class="dls-pill">New</span></div> Make sure there’s no loss of content or functionality if the user changes [<a href='https://www.w3.org/WAI/WCAG21/Understanding/text-spacing.html' target='_blank'>1.4.12</a>]:
      <ul class="standard-list">
        <li>Text line height/spacing to 1.5 times the font size (design system default).</li>
        <li>Paragraph spacing to 2 times the font size (design system default).</li>
        <li>Word spacing to .16 times the font size.</li>
        <li>Letter spacing to .12 times the font size.</li>
      </ul>

      <li>Don’t refer to shape, size, or visual location in text instructions (avoid things like ‘Click the square icon to continue’). Statements such as "choose one of the links below" or "all of the above" are OK, but use with caution [<a href='https://www.w3.org/WAI/WCAG21/Understanding/sensory-characteristics.html' target='_blank'>1.3.3</a>].</li>
      <li>Check out the <a href='/foundations/text-font/'>text and font foundation</a> for more on how to approach this.</li>
    </li>
  </ul>
</div>
<div class='container'>
  <h2><a id='accessibility-forms'></a>When designing forms and tables...</h2>
  <ul class="standard-list">
    <li>If you indicate mandatory fields, explain what the indicator means [<a href='https://www.w3.org/WAI/WCAG21/Understanding/info-and-relationships.html' target='_blank'>1.3.1</a>].</li>
    <li>Make sure elements with the same functionality are identified in the same way in all places (e.g. a search box is always labelled ‘Search’) [<a href='https://www.w3.org/WAI/WCAG21/Understanding/consistent-identification.html' target='_blank'>3.2.4</a>].</li>
    <li>Try to keep labels short and precise.</li>
    <li>Use labelling, instruction text, default content, tooltips, and other techniques so users know what data, and format might be expected [<a href='https://www.w3.org/WAI/WCAG21/Understanding/labels-or-instructions.html' target='_blank'>3.3.2</a>].</li>
    <li>Design your tables with headings for each column, and only use tables for relational data, and never for layout purposes [<a href='https://www.w3.org/WAI/WCAG21/Understanding/info-and-relationships.html' target='_blank'>1.3.1</a>].</li>
    <li>Check out the <a href='/patterns/forms/'>form pattern</a> and <a href='/components/tables/'>table component</a> for more on how to approach this.</li>
  </ul>
</div>
<div class='container'>
  <h2><a id='accessibility-focus'></a>When designing interactivity...</h2>
  <ul class="standard-list">
    <li>Make sure it’s always clear which element has focus as the user tabs through the page [<a href='https://www.w3.org/WAI/WCAG21/Understanding/focus-visible.html' target='_blank'>2.4.7</a>].</li>
    <li>When a user interacts with an element, make sure that confusing or disorienting changes don't take place, for example [<a href='https://www.w3.org/WAI/WCAG21/Understanding/on-focus.html' target='_blank'>3.2.1</a>, <a href='https://www.w3.org/WAI/WCAG21/Understanding/on-input.html' target='_blank'>3.2.2</a>]:
      <ul class="standard-list">
        <li>Don't submit a form automatically when a component receives focus.</li>
        <li>Don't open new windows when a component receives focus.</li>
        <li>Don't change focus to another component when one component receives focus.</li>
        <li>Only automatically move the user between input fields as they're completed if you inform them in text that this will take place first.</li>
        <li>Triggering tooltips and popovers on focus are OK, as long as they don't risk the user losing their place and context.</li>
        <li>Further form elements may be displayed when a radio button is selected, but the content of the whole page shouldn't change.</li>
      </ul>
    </li>
    <li><div class='dls-pill-container dls-badge badge-generic dls-badge-use'><span class="dls-pill">New</span></div> Design any elements that are triggered on hover or keyboard focus (e.g. error tooltips, dialogs, dropdown menus, datepickers) so that [<a href='https://www.w3.org/WAI/WCAG21/Understanding/content-on-hover-or-focus.html' target='_blank'>1.4.13</a>]:
      <ul class="standard-list">
        <li>The content that appears can be dismissed with the Esc key, or a close icon.</li>
        <li>For content that appears on hover, the pointer can be moved from the trigger to the new element without it disappearing.</li>
        <li>The content that appears remains visible until it loses focus, it’s dismissed, or no longer relevant (because the error is resolved).</li>
      </ul>
    </li>
    <li>When specifying mouse interactions [<a href='https://www.w3.org/WAI/WCAG21/Understanding/pointer-cancellation.html' target='_blank'>2.5.2</a>]:
      <ul class="standard-list">
        <li>Only action something with the mouse on its up-event (when the button is released, not when pressed down).</li>
        <li>If the user moves the pointer off the target before releasing, then cancel the action.</li>
        <li>Rarely, you might action something on the down-event, and reverse it on the up-event, for example, showing a video only when users 'press and hold'.</li>
      </ul>
    </li>
  </ul>
</div>
<div class='container'>
  <h2><a id='accessibility-errors'></a>When designing errors and recovery...</h2>
  <ul class="standard-list">
    <li>If the user enters information that isn't accepted, or required information is missing:
      <ul class="standard-list">
        <li>Indicate to the user that an error has occurred, with a message at a point in the flow that a user will discover it (e.g. a message next to the submit button, or by reloading the page and displaying an error message at the top) [<a href='https://www.w3.org/WAI/WCAG21/Understanding/error-identification.html' target='_blank'>3.3.1</a>].</li>
        <li>Show errors clearly inline [<a href='https://www.w3.org/WAI/WCAG21/Understanding/error-suggestion.html' target='_blank'>3.3.3</a>].</li>
        <li>Provide suggestions for correction and examples of valid data [<a href='https://www.w3.org/WAI/WCAG21/Understanding/error-suggestion.html' target='_blank'>3.3.3</a>].</li>
      </ul>
    </li>
    <li>If the user is making legal committment or financial transactions, or modifying or deleting data, at least one of the following must be provided [<a href='https://www.w3.org/WAI/WCAG21/Understanding/error-prevention-legal-financial-data.html' target='_blank'>3.3.4</a>]:
      <ul class="standard-list">
        <li>Ability to reverse</li>
        <li>checking for input errors</li>
        <li>ability to confirm before final</li>
      </ul>
    </li>
    <p>Check out the <a href='/patterns/errors-validation/'>errors and validation pattern</a> for more on how to approach this.</p>
  </ul>
</div>
