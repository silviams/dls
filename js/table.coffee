---
---

$checkbox = $('input.custom-checkbox')
$table_actions = $('.table .actions-container')

$checkbox.on "click", (e)->
  $(e.currentTarget).closest('.row').toggleClass "selected"

$table_actions.on "click", (e)->
  # Is the current row the one with actions-visible?
  $current_row = $(e.currentTarget).closest('.row')

  if $current_row.hasClass "actions-visible"

  else
    # remove any active row first
    $('.table .row.actions-visible').removeClass "actions-visible"

  # apply to new cell
  $current_row.toggleClass "actions-visible"

