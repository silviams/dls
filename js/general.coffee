---
---

# $sidebar = $('.sidebar-content')
# $navigation = $('.navigation')
# $primary_navigation = $('.navigation-container')
$primary_menu = $('.primary-menu')
$primary_menu_link = $('.primary-menu li')
$secondary_menu = $('.secondary-menu')
$copied_to_clipboard_toast = $('.system-message.system-toast.success-message.copied-to-clipboard')
$animated_toast_object = $('.system-message.system-toast.error-message.animated-toast-object')

#
# Window scroll events
#
$(window).scroll ->
  scrollAction()

###
# Events when page is scrolled
#
# @method scrollAction
###
scrollAction = ->
  if window.pageYOffset > 48
    $('body').addClass "sticky-navigation"
  else if window.pageYOffset < 48
    $('body').removeClass "sticky-navigation"

#
# Menu item click event
#
$primary_menu_link.on "click", (menu_item)->
  if $primary_menu.hasClass "visible"
    # remove current selected item
    $('.primary-menu li.selected').removeClass "selected"

    # Add selected to the selected menu item
    menu_item.currentTarget.classList.add "selected"

    # Hide the primary menu
    $primary_menu.removeClass "visible"

    # Show the correct secondary menu
    target_menu_id = menu_item.currentTarget.dataset.targetMenu

    target_menu = document.getElementById(target_menu_id)

    $('.secondary-menu.visible').removeClass "visible"

    target_menu.classList.add "visible"

  else
    $primary_menu.addClass "visible"



$tab_menu = $('.tab-menu')
$tab_menu_link = $('.tab-menu li')
$secondary_tab = $('.secondary-tab')

$tab_menu_link.on "click", (tab_item)->
  target_tab_id = tab_item.currentTarget.dataset.targetTabs
  target_tab = document.getElementById(target_tab_id)

  $('.tab-menu li.selected').removeClass "selected"
  $('.secondary-tab.visible').removeClass "visible"

  tab_item.currentTarget.classList.add "selected"
  target_tab.classList.add "visible"

#
# Method to update menu
#
CheckNav = () ->
  primary_menu = $('body').data('primaryMenu')
  secondary_menu = $('body').data('secondaryMenu')

  # set the correct primary menu
  $('.primary-menu li.selected').removeClass "selected"
  $(".primary-menu li[data-target-menu='menu-#{primary_menu}']").addClass "selected"

  # show the correct secondary menu
  $('.secondary-menu.visible').removeClass "visible"
  $("#menu-#{primary_menu}").addClass "visible"

CheckNav()


#
# Delay method to reuse
#
delay = (ms, func) -> setTimeout func, ms


#
# Copy on click
#
$('.copy-to-clipboard').on 'click', ->
  # Get the text to copy
  value = $(this).data('clipboard-text')

  # create dynamic input
  $temp = $('<input>')

  # Add input to page
  $('body').append $temp

  # update value of input and select it
  $temp.val(value).select()

  # Copy to clipboard
  document.execCommand 'copy'

  # remove the input
  $temp.remove()


  $copied_to_clipboard_toast.addClass "animate-in"

  delay 3000, ->
    $copied_to_clipboard_toast.removeClass "animate-in"


#
# Animate toast in/out Object
#
$('.animate-toast').on 'click', ->

  $animated_toast_object.addClass "animate-in"

  delay 3000, ->
    $animated_toast_object.removeClass "animate-in"




# Keypress Events
$(document).on 'keyup', (e) ->
  tag = e.target.tagName.toLowerCase()

  # Escape Key
  if e.which == 27
    $primary_menu.removeClass "visible" if $primary_menu.hasClass "visible"

if document.getElementById('gradient') != null
  granimInstance = new Granim(
    element: '#gradient'
    direction: 'diagonal'
    opacity: [
      1
      1
      1
    ]
    isPausedWhenNotInView: true
    stateTransitionSpeed: 500
    states:
      'default-state':
        gradients: [
          [
            '#582C83'
            '#0077C8'
            '#00A376'
          ]
          [
            '#00A376'
            '#0077C8'
            '#582C83'
          ]
        ]
        transitionSpeed: 10000
      'foundations-state':
        gradients: [
          [
            '#00A376'
            '#00A376'
            '#00A376'
          ]
        ]
      'components-state':
        gradients: [
          [
            '#0077C8'
            '#0077C8'
            '#0077C8'
          ]
        ]
      'heroes-state':
        gradients: [
          [
            '#582C83'
            '#582C83'
            '#582C83'
          ]
        ]
      'content-state':
        gradients: [
          [
            '#003349'
            '#003349'
            '#003349'
          ]
        ]
  )

$('.foundations-segment .segment-link').hover (->
  granimInstance.changeState 'foundations-state'
), ->
  granimInstance.changeState 'default-state'

$('.components-segment .segment-link').hover (->
  granimInstance.changeState 'components-state'
), ->
  granimInstance.changeState 'default-state'

$('.hero-flows-segment .segment-link').hover (->
  granimInstance.changeState 'heroes-state'
), ->
  granimInstance.changeState 'default-state'

$('.content-segment .segment-link').hover (->
  granimInstance.changeState 'content-state'
), ->
  granimInstance.changeState 'default-state'


#
# Accordion expand
#
$accordion_tab = $('.accordion .accordion-tab')

$accordion_tab.on "click", ->
  $(@).toggleClass "open"


#
# Action popover
#
$action_popover_trigger = $('.ap-icon')

$action_popover_trigger.on "click", (e)->
  $(e.currentTarget).next(".ap-container").toggleClass "ap-visible"
  $(e.currentTarget).toggleClass "ap-active"

# Toggle on change
$("#cb-created-t").change ->
  $(".cr-show").toggle()

# Toggle on change
$("#cb-num-t").change ->
  $(".nu-show").toggle()

# Toggle on change
$("#cb-customer-t").change ->
  $(".cu-show").toggle()

# Toggle on change
$("#cb-total-t").change ->
  $(".to-show").toggle()


#
# Dialog
#
$show_dialog = $('.show-dialog')
$hide_dialog = $('.dialog-close')

#
# Show dialog on button click
#
$show_dialog.on "click", ->
  $('.dialog-component').addClass "showdialog"
  $('body').addClass "stop-scroll"

#
# Hide dialog on button click or close
#
$hide_dialog.on "click", ->
  $('.dialog-component').removeClass "showdialog"
  $('body').removeClass "stop-scroll"


#
# FS Dialog
#
$show_fs_dialog = $('.show-fs-dialog')
$hide_fs_dialog = $('.fs-dialog-close')

#
# Show dialog on button click
#
$show_fs_dialog.on "click", ->
  $('.fs-dialog-component').addClass "showdialog"
  $('body').addClass "stop-scroll"

#
# Hide dialog on button click or close
#
$hide_fs_dialog.on "click", ->
  $('.fs-dialog-component').removeClass "showdialog"
  $('body').removeClass "stop-scroll"

  delay 1500, ->
    $('.fs-dialog').removeClass "expanded"
    $('.fs-dialog').removeClass "minimised"
    $('.dialog-background').removeClass "minimised-bg"

#
# FS Dialog Expand
#
$expand_fs_dialog = $('.expand-fs-dialog')

#
# Expand dialog on button click
#
$expand_fs_dialog.on "click", ->
  $('.fs-dialog').toggleClass "expanded"


#
# FS Dialog Minimise
#
$minimise_fs_dialog = $('.minimise-fs-dialog')

#
# Minimise dialog on button click
#
$minimise_fs_dialog.on "click", ->
  $('.fs-dialog').toggleClass "minimised"
  $('.dialog-background').toggleClass "minimised-bg"
  $('body').toggleClass "stop-scroll"


#
# Wizard
#
$wizard_next_page = $('.next-page')
$wizard_previous_page = $('.previous-page')
$wizard_active = $('.wizard-content .tab-pane-active')
$progress_indicator_active = $('.wizard-progress-indicator .progressbar .active')
$progress_line = $('.wizard-progress-indicator .progressbar .progress-line .progress-line-current')
$progress_line_remove = $('.progress-last-remove')


#
# Wizard next page
#
$wizard_next_page.on "click", ->

  # Change content
  $('.tab-pane-active').removeClass "tab-pane-active"
  $wizard_active.next().addClass "tab-pane-active"
  $wizard_active = $('.wizard-content .tab-pane-active')

  # Change step on progress bar
  $('.active').addClass "completed"
  $('.active').removeClass "active"
  $progress_indicator_active.nextAll("li").eq(0).removeClass "completed"
  $progress_indicator_active.nextAll("li").eq(0).addClass "active"
  $progress_indicator_active = $('.wizard-progress-indicator .progressbar .active')
  $disabled_step = $('.wizard-progress-indicator .progressbar .active .link')
  $disabled_step.removeClass "disabled"

  # Show 'previous' button
  $wizard_previous_page.addClass "previous-page-visible"

  # Change progress line
  $progress_line_remove.removeClass "progress-line-disabled progress-line-current"
  $progress_line_remove.addClass "progress-line-completed"
  $progress_line_remove.parent().nextAll("svg").eq(0).children().addClass "progress-line-last progress-last-remove progress-line-current"
  $progress_line_remove.removeClass "progress-line-last progress-last-remove progress-line-current"
  $progress_line_remove = $('.progress-last-remove')

#
# Wizard next page first click
#
$wizard_next_page.one "click", ->
  $progress_line.addClass "progress-line-last progress-last-remove"
  $progress_line_remove = $('.progress-last-remove')


#
# Wizard previous page
#
$wizard_previous_page.on "click", ->
  $('.tab-pane-active').removeClass "tab-pane-active"
  $wizard_active.prev().addClass "tab-pane-active"
  $wizard_active = $('.wizard-content .tab-pane-active')
  $('.active').addClass "completed"
  $('.active').removeClass "active"
  $progress_indicator_active.prevAll("li").eq(0).removeClass "completed"
  $progress_indicator_active.prevAll("li").eq(0).addClass "active"
  $progress_indicator_active = $('.wizard-progress-indicator .progressbar .active')


#
# Datepicker
#
$('#datepicker').datepicker
  showOtherMonths: true
  selectOtherMonths: true
  dateFormat: 'dd/mm/yy'
  dayNamesMin: [
    'Sun'
    'Mon'
    'Tue'
    'Wed'
    'Thu'
    'Fri'
    'Sat'
  ]


#
# Date range
#
$ ->

  getDate = (element) ->
    date = undefined
    try
      date = $.datepicker.parseDate(dateFormat, element.value)
    catch error
      date = null
    date

  from = $('#from').datepicker(
    defaultDate: '+1w'
    numberOfMonths: 2
    showOtherMonths: true
    selectOtherMonths: true
    dateFormat: 'dd/mm/yy'
    dayNamesMin: [
      'Sun'
      'Mon'
      'Tue'
      'Wed'
      'Thu'
      'Fri'
      'Sat'
    ]).on('change', ->
    to.datepicker 'option', 'minDate', getDate(this)
    return
  )
  to = $('#to').datepicker(
    defaultDate: '+1w'
    numberOfMonths: 2
    showOtherMonths: true
    selectOtherMonths: true
    dateFormat: 'dd/mm/yy'
    dayNamesMin: [
      'Sun'
      'Mon'
      'Tue'
      'Wed'
      'Thu'
      'Fri'
      'Sat'
    ]).on('change', ->
    from.datepicker 'option', 'maxDate', getDate(this)
    return
  )
  return


#
# Sidebar
#
$show_sidebar = $('.show-sidebar')
$hide_sidebar = $('.sidebar-close')

#
# Show sidebar on button click
#
$show_sidebar.on "click", ->
  $('.sidebar-component').addClass "showsidebar"
  $('body').addClass "stop-scroll"

#
# Hide dialog on button click or close
#
$hide_sidebar.on "click", ->
  $('.sidebar-component').removeClass "showsidebar"
  $('body').removeClass "stop-scroll"


# Dropdown Code
dropdown_input = $('.dropdown-list-container .dropdown-list-input')

dropdown_input.on "focus", ->
  # hide all visible content first
  $('.dropdown-list-container .dropdown_list_content').removeClass "visible"

  dropdown_list = $(@parentElement)
  dropdown_content = dropdown_list.find '.dropdown_list_content'

  # dropdown_list_content visible
  dropdown_content.addClass "visible"


# Dropdown List Item
dropdown_list_item = $('.dropdown_list .dropdown_list_item')


dropdown_list_item.on "click", ->
  # Find parent container
  dropdown_container = $(this).closest('.dropdown-list-container')

  # find the input
  dropdown_input = dropdown_container.find(".dropdown-list-input")

  # give the input the clicked text value
  dropdown_input.val @textContent
  dropdown_input.data('id', $(this).data('id'))


###
# Hide Dropdown Content
###
HideDropdownListContent = () ->
  $('.dropdown-list-container .dropdown_list_content').removeClass "visible"


###
# Events when a user clicks on the screen
###
$(window).click (e) ->
  in_dropdown_list_input = e.target.classList.contains('dropdown-list-input')

  if in_dropdown_list_input is true
    return
  else
    HideDropdownListContent()


#
#
# Specific Dropdown
dropdown_list_item_hero = $('.hero_dropdown .dropdown_list_item')
dropdown_list_item_customer = $('.customer_dropdown .dropdown_list_item')

dropdown_list_item_hero.on "click", ->
  $('.hero-content-container').hide()
  $('#' + $(this).data('id')).show()

dropdown_list_item_customer.on "click", ->
  $('.customer-story-container').hide()
  $('#' + $(this).data('id')).show()
