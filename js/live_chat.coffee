---
---

$web_chat_show_pop_up = $('.show-live-chat-timed-popup')

$web_chat_trigger = $('.live-chat-trigger')

# The primary dialog
$web_chat_dialog = $('.live-chat-dialog')

# Close dialog button
$web_chat_dialog_close = $('.live-chat-dialog .close')

# Customer name input
$web_chat_name_input = $('#web_chat_name')

# Connect to agent button
$web_chat_connect_button = $('.live-chat-dialog .web-chat-connect')
$web_chat_customer_reply_textarea = $('.live-chat-dialog #customer_reply')
$web_chat_message_interface = $('.live-chat-dialog .message-interface')
$web_chat_start_chat_popup_button = $('.live-chat-dialog .start-chat')
$web_chat_end_chat_button = $('.chat-end-trigger')
$web_chat_confirm_close_in_queue = $('.live-chat-dialog .close-in-queue')
$web_chat_confirm_close_has_typed = $('.live-chat-dialog .close-has-typed')
$web_chat_confirm_dialog_cancel = $('.live-chat-dialog .confirm-dialog .cancel-option')
$web_chat_confirm_dialog_close = $('.live-chat-dialog .confirm-dialog .close-option')

# global variables
# These are used to handle confirm dialog display options
in_queue = false
has_typed = false

$web_chat_show_pop_up.on "click", ->
  delay 1000, ->
    $web_chat_dialog.addClass "live-chat-prompt live-chat-visible"

$web_chat_confirm_dialog_cancel.on "click", ->
  $('.live-chat-dialog .confirm-dialog.visible').removeClass 'visible'

$web_chat_confirm_dialog_close.on "click", ->
  close_chat()

$web_chat_end_chat_button.on "click", ->
  $web_chat_dialog.addClass "web-chat-end-session"

  has_typed = false
  document.getElementById("customer_reply").disabled=true
  document.getElementById("send_button").disabled=true

  current_time = formatAMPM(new Date())
  $web_chat_message_interface.append("<div class='closed-chat-segment'><div class='closed-chat-timestamp'>Chat closed by team member - <span class='time-stamp agent-closed'>#{current_time}</span></div><p class='chat-download-text'>We thought you might like a record of our chat today.</p><div class='download-transcript'>Download transcript</div></div>");

  delay 50, ->
    container = $('.live-chat-dialog .chat-message-container')
    height = $web_chat_message_interface.height()
    container.scrollTop height


$web_chat_start_chat_popup_button.on "click", ->
  $web_chat_dialog.addClass "live-chat-visible"
  $web_chat_dialog.removeClass "live-chat-prompt"

  delay 50, ->
    document.getElementById("web_chat_name").value = ""
    document.getElementById("web_chat_name").focus()


$web_chat_customer_reply_textarea.on 'change', (e)->
  has_typed = true
  current_time = formatAMPM(new Date())

  $web_chat_message_interface.append("<div class='customer-reply'><div class='message-reply'>#{@value}<div class='time-stamp'>#{current_time}</div></div></div>");
  @value = ""

  delay 50, ->
    document.getElementById("customer_reply").focus()

    container = $('.live-chat-dialog .chat-message-container')
    height = $web_chat_message_interface.height()
    container.scrollTop height


# Closing the web chat dialog
$web_chat_dialog_close.on "click", ->

  if in_queue is false and has_typed is false
    close_chat()
  else if in_queue is true
    $web_chat_confirm_close_in_queue.addClass 'visible'
  else if has_typed is true
    $web_chat_confirm_close_has_typed.addClass 'visible'

close_chat = () ->
  in_queue = false
  has_typed = false
  $web_chat_dialog.removeClass "live-chat-visible"
  $web_chat_dialog.removeClass "live-chat-prompt"

  delay 100, ->
    $web_chat_dialog.removeClass "web-chat-loading web-chat-connected web-chat-queued"
    $('.live-chat-dialog .confirm-dialog.visible').removeClass 'visible'

# As a user types their name
$web_chat_name_input.on "keyup", (e) ->
  checkInputValue($web_chat_name_input)

# Show web chat dialog
$web_chat_trigger.on "click", ->
  $web_chat_dialog.addClass "live-chat-visible"
  $web_chat_dialog.removeClass "web-chat-prompt"

  delay 50, ->
    document.getElementById("web_chat_name").value = ""
    document.getElementById("web_chat_name").focus()

# Connect to agent
$web_chat_connect_button.on "click", ->
  $web_chat_dialog.addClass "web-chat-loading"

  delay 3000, ->
    current_time = formatAMPM(new Date())
    $('.live-chat-dialog .agent-reply .time-stamp.current-time').html current_time

    # Get first name and add to reply
    text  = document.getElementById("web_chat_name").value
    arr   = text.split(" ")
    first = arr.shift()
    document.getElementById("first_name").innerHTML = first

    # show connected state and focus on reply
    $web_chat_dialog.addClass "web-chat-connected"
    document.getElementById("customer_reply").focus()

checkInputValue = (input) ->
  status = input.val() is ""
  $web_chat_connect_button.prop('disabled', status)


# Pressing Enter in textarea
input = document.getElementById('customer_reply')

if input
  input.addEventListener 'keyup', (event) ->
    # Cancel the default action, if needed
    event.preventDefault()
    # Number 13 is the "Enter" key on the keyboard
    if event.keyCode == 13
      # Trigger the button element with a click
      $web_chat_customer_reply_textarea.trigger "change"
    return

#
# SetTimeout
#
delay = (ms, func) -> setTimeout func, ms

#
# Format Date
#
formatAMPM = (date) ->
  hours = date.getHours()
  minutes = date.getMinutes()
  ampm = if hours >= 12 then 'pm' else 'am'
  hours = hours % 12
  hours = if hours then hours else 12
  # the hour '0' should be '12'
  minutes = if minutes < 10 then '0' + minutes else minutes
  strTime = hours + ':' + minutes + ' ' + ampm
  strTime
