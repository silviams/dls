---
---

containerSelector = '.table-sortable'
containers = document.querySelectorAll(containerSelector)
sortable = new Draggable.Sortable(containers,
  draggable: '.row-sortable'
  mirror:
    appendTo: containerSelector
    constrainDimensions: true)
