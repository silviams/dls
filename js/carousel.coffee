---
---

$carousel_demo = $('.carousel-demo')
$carousel_demo_prev = $('.carousel-navigate.nav-prev')
$carousel_demo_next = $('.carousel-navigate.nav-next')

$carousel_demo_pip = $('.carousel-pip')


# Allow pips to control active slide
$carousel_demo_pip.on "click", ->
  number = parseInt(this.attributes["active-card"].value)
  $carousel_demo.attr "active-card", number

  checkDisable number


$carousel_demo_prev.on "click", ->
  # get current slide active
  current = parseInt($carousel_demo.attr "active-card")
  prev = current - 1

  # make next slide active
  $carousel_demo.attr "active-card", prev

  checkDisable prev


$carousel_demo_next.on "click", ->
  # get current slide active
  current = parseInt($carousel_demo.attr "active-card")
  next = current + 1

  # make next slide active
  $carousel_demo.attr "active-card", next

  checkDisable next

checkDisable = (card)->
  # if first card disable prev button
  if card is 1
    $carousel_demo_prev.addClass "disable"
    $carousel_demo_next.removeClass "disable"
  # if last card disable next button
  else if card is 3
    $carousel_demo_prev.removeClass "disable"
    $carousel_demo_next.addClass "disable"
  else
    $carousel_demo_prev.removeClass "disable"
    $carousel_demo_next.removeClass "disable"
