---
---

delay = (ms, func) -> setTimeout func, ms

window.data = $.getJSON('/search_data.json')

####################
# After Json loads build our data array
####################
window.data.then (json) ->
  array = []
  for key of json
    if json.hasOwnProperty(key)
      item = json[key]

      array.push
        id: item.id
        url: item.url
        title: item.title
        content: item.content
        summary: item.summary
        tags: item.tags

  # Set our usable array on the window
  window.search_data = array

  window.idx = lunr(->
    @ref 'id'
    @field 'tags', boost: 10
    @field 'title', boost: 50
    @field 'content'

    window.search_data.forEach ((data) ->
      @add data
    ), this
  )


####################
# As user types to search
####################
$('#search-box').keyup (event) ->
  event.preventDefault()

  query = $('#search-box').val()
  $('#search-results').empty()

  unless query is "" or query.length < 4
    results = window.idx.search(query)

    # Only return top 8
    results = results.slice(0,8)

    if results.length
      results.forEach (result) ->
        buildSearchResult result
    else
      buildSearchResult "none"

    $('#search-results .search-result').hover (e) ->
      $('#search-results .search-result.selected').removeClass("selected").blur()
      $(e.currentTarget).addClass("selected").focus()


####################
# Listen to global keyups
####################
$(document).on 'keyup', (e) ->
  # Escape key
  if e.which is 27
    HideGlobalSearch()

  else if e.ctrlKey && e.which is 83
    # Ctrl + S
    ShowGlobalSearch()


###
# Events when page is scrolled
#
# @method HideGlobalSearch
###
HideGlobalSearch = () ->
  $('.global-search.visible').removeClass "visible"
  $('body.global-search-active').removeClass "global-search-active"
  $('#search-box').val('')
  $('#search-results').empty()


# ###
# # Events when page is scrolled
# #
# # @method scrollAction
# ###
ShowGlobalSearch = () ->
  $('body').addClass "global-search-active"
  $('.global-search').addClass "visible"
  $(':focus').blur()

  delay 100, ->
    $('#search-box').focus()


# Action Bar click for global search
$('.header .action-global-search').on 'click', (e) ->
  ShowGlobalSearch()


####################
# Add our listener for up and down arrows
####################
$('.global-search').on 'keyup', (e) ->
  # Keys to watch
  up_arrow = 38
  down_arrow = 40
  enter_key = 13

  keypress = e.which

  ####################
  #  ARROW - UP
  ####################
  if keypress is up_arrow
    MoveSelection 'up'

  ####################
  #  ARROW - DOWN
  ####################
  else if keypress is down_arrow
    MoveSelection 'down'


# ###
# # Events when page is scrolled
# #
# # @method MoveSelection
# ###
MoveSelection = (direction) ->
  # Have we selected a result already?
  selected = $('#search-results .search-result.selected')
  result_selected = selected.length > 0

  # Select the next one in the list
  if result_selected
    # Direction to call
    if direction is 'up'
      next_result = selected.prev()
    else if direction is 'down'
      next_result = selected.next()

    selected.removeClass "selected"
    selected.blur()

    if next_result.length > 0
      next_result.addClass "selected"
      next_result.focus()
    else
      $('#search-box').focus()

  # Not yet seleceted, select the first one
  else
    $('#search-box').blur()
    $('#search-results a').first().addClass("selected").focus()


###
# Events when page is scrolled
# @method buildSearchResult
###
buildSearchResult = (result) ->

  if result is "none"
    render = "<div class='no-result'>Sorry, nothing found for this term.</div>"
  else
    result = window.search_data[result.ref]

    render = "<a class='search-result' href='#{result.url}'><div class='search-result-title'>#{result.title}</div><div class='search-result-summary'>#{result.summary}</div></a>"
  $('#search-results').append render


###
# Events when a user clicks on the screen
###
$(window).click (e) ->
  in_global_search = e.target.classList.contains('global-search')
  in_global_search_input = e.target.classList.contains('search-box')
  in_global_search_trigger = e.target.classList.contains('icon-search')

  if in_global_search or in_global_search_input or in_global_search_trigger is true
    return
  else
    HideGlobalSearch()
