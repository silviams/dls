---
layout: default
secondary_menu_id: navigation
primary_menu_id: components
title: Navigation
sketch_file: /downloads/components/navigation/navigation_interim.sketch
sketch_file_version: 0.1
summary: The main way users move around your app. Perhaps the most important pattern of all.
tags: nav, navigation, primary nav, primary navigation, IA, information architecture, top nav, left nav, layout
---

<p class='summary'>{{ page.summary }}</p>
<div class='content-tab'>
  <ul class='tab-menu'>
    <li class='selected' data-target-tabs='tabs-1'>Design</li>
    <li data-target-tabs='tabs-3'>Guidance</li>
  </ul>
</div>
<div class='secondary-tab visible' id='tabs-1'>
  <div class="system-message info-message">
    <div class='system-message-title'>Accessibility</div>
    <div class='system-message-body'>WCAG 2.1 includes requirements aimed at navigation - see the <a href='/foundations/accessibility#accessibility-links'>Accessibility</a> section for more info.</div>
  </div>
  <div class='container'>
    <p>When it comes to designing for users, context is king - navigation that works well in one context, may not work as well in another. That's why we use both vertical and horizontal navigation in different contexts.</p>
    <p>Navigation decisions are critical to usability and user experience. We recommend carefully testing and adapting these patterns with rationale, to get the best fit for your users.</p>
    <p>Your choice of navigation will also affect your general layout</a> and <a href='/components/header/'>Header Bar Pattern</a>, so make a choice considering all of these together.</p>
  </div>
  <div class='container'>
    <h2>Navigation Options</h2>
    <ul class="standard-list">
      <li>We use <span class='emphasis-term'>left navigation for web apps like Accounting and Enterprise Management</span>, or for very simple web portals like this one.
        <ul class="standard-list">
          <li><span class='emphasis-term'>Flat Left Nav</span> - single level navigation for apps like Accounting. Can be expanded or collapsed to save room.</li>
          <li><span class='emphasis-term'>Advanced Left Nav</span> - offering three levels of hierarchy for apps like Enterprise Management.</li>
          <li><span class='emphasis-term'>Colour Variants</span> - following the accent colours for small, medium, and larger businesses.</li>
        </ul>
      </li>
      <li>We use <span class='emphasis-term'>top navigation for websites like Sage.com</span>, leaving the full page width free for compelling marketing designs.</li>
    </ul>
  </div>
  <div class='container'>
    <h2>Flat Left Nav</h2>
    <p>If you choose Flat Left nav, then your app will need to have a header bar at the top of the screen too. See the <a href='/components/header/'>Header Bar Pattern</a>.</p>
    <div class='pod'>
      <img src='/visuals/nav-flat.png'/>
      <h3>Collapsed View</h3>
      <p>This is useful if a user just wants to make best use of space, or to support responsive design to tablet (portrait) size.</p>
      <img src='/visuals/nav-collapsed.png'/>
    </div>
  </div>
  <div class='container'>
    <h2>Advanced Left Nav</h2>
    <p>If you choose advanced left nav, then the functions found in the top <a href='/components/header/'>Header Bar</a> are found on the left instead. You don't need a header object, or you can use it for something else.</p>
    <div class='pod'>
      <img src='/visuals/nav-advanced.png'/>
    </div>
  </div>
  <div class='container'>
    <h2>Top Nav for Websites</h2>
    <p>A special top navigation pattern is available for websites only - like Sage.com.</p>
    <div class='pod full-width'>
      <img src='/visuals/nav-website.jpg'/>
    </div>
  </div>
</div>
<div class='secondary-tab' id='tabs-3'>
  <div class='container'>
    <h2>Have I got the right pattern?</h2>
    <ul class="standard-list">
      <li>Need a header bar? See the <a href='/components/header/'>Header Pattern</a>.</li>
      <li>Thinking about mobile navigation? See the <a href='/mobile/'>Mobile section</a>.</li>
    </ul>
  </div>
  <div class='container'>
    <h2>What configurations are available?</h2>
    <ul class="standard-list">
      <li>Left Navigation for Web Apps: Simple, Advanced</li>
      <li>Top Navigation for websites</li>
      <li>Colour theming by product accent colour</li>
    </ul>
  </div>
  <div class='container'>
    <h2>Top Nav or Left Nav - Which Works Better?</h2>
    <p>When it comes to designing for users, context is king - navigation that works well in one context, may not work as well in another. That's why we use both vertical and horizontal navigation in different contexts.</p>
    <h3>Left navigation</h3>
    <ul class="standard-list">
      <li>Left navigation may be faster and more efficient, because users can <a href='http://uxmovement.com/navigation/top-navigation-vs-left-navigation-which-works-better/' target='_blank'>visually scan more items</a> in a single eye fixation vertically than horizontally.</li>
      <li>Some <a href='https://www.usability.gov/get-involved/blog/2006/04/left-navigation-is-best.html' target='_blank'>formal studies</a>, although small and conducted a while ago, suggest left navigation has human performance and preference advantages.</li>
      <li>You can have more items, and longer labels without running out of room. Consider languages with typically longer words like German in particular. But this may mean designers become less strict about keeping information architecture simple.</li>
      <li>Left vertical navigation falls into the long stroke of the F-shape, in which readers look first across the top and then down the left side as a natural reading pattern.</li>
    </ul>
    <h3>Top navigation</h3>
    <ul class="standard-list">
      <li>Keep labels short, and limit the number of items, to avoid running out of room. Always avoid horizontal scroll.</li>
      <li>Navigation items aren't felt to have equal visual weight. The left-most items may be perceived as more important, as they're in the primary optical area (top left). But, this can be used to commercial advantage.</li>
      <li>To handle lots of items, you may need to use techniques like submenus on hover. However, anything other than overt flat navigation tends to result in a measurable drop-off, since clicks and hovers to reveal items mean user effort and decision-making. Discoverability is also impaired.</li>
      <li>However, large, rectangular menus that group navigation options to eliminate scrolling and use typography, icons, and tooltips to explain users' choices, can work well in a website or marketing context (<a href='https://www.nngroup.com/articles/mega-menus-work-well/' target='_blank'>Nielsen, 2017</a>). Context is very different for frequent use web apps, where this technique is not recommended.</li>
      <li>Top navigation does allow a designer to build visually compelling, full-width designs, which can have marketing advantages.</li>
    </ul>
  </div>
  <div class='container'>
    <h2>Guidance, hints and tips</h2>
    <ul class="standard-list">
      <li>Strive for simple, flat navigation. It's easiest for users.</li>
      <li>Don’t use images or icons alone as your main method of navigation - it’s difficult for a user to know exactly what they’ll get. Steve Krug calls this 'mystery meat' navigation.</li>
      <li>Left align the text of navigation items, so the user's eyes can move in a straight line and they don't have to re-acquire the beginning of each new line.</li>
      <li>Start each menu item with the one or two most information-carrying words.</li>
      <li>Avoid using the same few words to start list items, because this makes them harder to scan.</li>
    </ul>
  </div>
</div>
