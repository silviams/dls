---
layout: default
secondary_menu_id: dialogs
primary_menu_id: components
title: Dialogs
sketch_file: /downloads/components/dialogs.sketch
sketch_file_version: 0.7
summary: Present errors, decisions or acknowledgments that need attention before the user can continue.
tags: dialog, dialogs, modal, modals, container, containers
---

<p class='summary'>{{ page.summary }}</p>
<div class='content-tab'>
  <ul class='tab-menu'>
    <li class='selected' data-target-tabs='tabs-1'>Design</li>
    <li data-target-tabs='tabs-3'>Guidance</li>
  </ul>
</div>
<div class='secondary-tab visible' id='tabs-1'>
  <ul class="standard-list">
    <li>Dialogs are available in 7 standard sizes. No matter the size, they're always positioned in the centre of the viewport.</li>
    <li>Dialogs are used for errors, decisions or acknowledgments that need attention before the user can continue. They interrupt the user, so use them sparingly.</li>
    <li>Buttons are usually positioned at the bottom right of a page or dialog. In a pair, Cancel goes on the left, and Save goes on the right.</li>
    <li>Avoid using dialogs for confusing ‘Yes’/’No’ choices. Instead, name the actions descriptively, for example ‘Save Changes’ vs. ‘Continue Editing’.</li>
    <li>A dialog is usually a surface on top of all other surfaces - they shouldn’t be obscured by other elements or appear partially on screen - avoid placing a dialog on top of another dialog.</li>
  </ul>
  <div class='container'>
    <button class='dls-button button-small generic-biz primary show-dialog'>Try Me</button>
  </div>
  <div class='container'>
    <h2>Single Action</h2>
    <p>If a single action is provided, it must be an acknowledgement action. A good example is an alert.</p>
    <img src='/visuals/dialog-single.png'/>
  </div>
  <div class='container'>
    <h2>Two Actions</h2>
    <p>If two actions are provided, either:
      <ul class='standard-list'>
        <li>One must be a confirming action, and the other a dismissing action (a good example is a warning).</li>
        <li>It must be a choice between two courses of action.</li>
      </ul>
    </p>
    <img src='/visuals/dialog-two.png'/>
  </div>
  <div class='container'>
    <h2>More Than Two Actions</h2>
    <p>If you need more options, consider using radio buttons with a confirm and dismiss. This will give you more space for text to be precise about the course of action.</p>
    <img src='/visuals/dialog-three.png'/>
  </div>
  <div class='container'>
    <h2>Examples of Dialog sizing variations</h2>
    <img class='example-img' src='/visuals/dialog-sizes.png'/>
  </div>
  <div class='container'>
    <h2>Examples of Dialog with a sticky footer</h2>
    <p>Sometimes a dialog might contain content that exceeds the vertical height of the user's monitor. In this instance, the primary and secondary buttons within the dialog are contained within a sticky footer that sits above this content. A scrollbar is also situated to the right of the dialog.</p>
    <img class='example-img' src='/visuals/dialog-stickyfooter.png'/>
  </div>
  <div class='container'>
    <h2>Background Mask</h2>
    <p>A background mask is created by applying opacity: rgba(0,20,29,0.6)</p>
    <div class='mask-example'>
      <img src='/visuals/screenshots/app-tablet.png'/>
      <div class='mask-example-background'>
      </div>
    </div>
  </div>
</div>
<div class='secondary-tab' id='tabs-3'>
  <div class='container'>
    <h2>Have I got the right component?</h2>
    <ul class="standard-list">
      <li>Complex task that needs more space, or need to place a dialog on top of a dialog? Try a <a href='/components/full-screen-dialogs'>full screen dialog</a>.</li>
      <li>Need to interact with the underlying page at the same time? Try a <a href='/components/sidebar'>sidebar</a>.</li>
      <li>More than 2 courses of action? Use <a href='/components/radios/'>radio buttons</a> with a single confirmation action.</li>
      <li>Not an error that blocks a tasks, and no decision or acknowledgement required? Try a passive <a href='/components/messages/'>message</a> on the background page.</li>
    </ul>
  </div>
  <div class='container'>
    <h2>What configurations are available?</h2>
    <ul class="standard-list">
      <li>7 standard sizes</li>
      <li>Options: Sticky header; Sticky footer; Pin to browser bottom.</li>
      <li>States: Error format; Warning format; Acknowledgment format.</li>
    </ul>
  </div>
  <div class='container'>
    <h2>Guidance, hints and tips</h2>
    <ul class="standard-list">
      <li>Dialogs are used for errors, decisions or acknowledgments that need attention before the user can continue. They interrupt the user, so use them sparingly.</li>
      <li>Dialogs typically seek confirmation (of a decision), dismissal (of an error), or acknowledgement (of some information).</li>
      <li>It's good practice for dialogs to have no more than two actions:
        <ul class="standard-list">
          <li>If a single action is provided, it must be an acknowledgement action. A good example is an alert.</li>
          <li>If two actions are provided, either:
            <ul class='standard-list'>
              <li>One must be a confirming action, and the other a dismissing action (a good example is a warning).</li>
              <li>It must be a choice between two courses of action.</li>
            </ul>
          </li>
          <li>Providing a third action such as 'Learn more' is not recommended as it leaves the task unfinished.</li>
          <li>If you need more options, consider using <a href='/components/radios/'>radio buttons</a> with a confirm and dismiss. This will give you more space for text to be precise about the course of action.</li>
          <li>Avoid confusing ‘Yes’/’No’ choices. Instead, name the actions descriptively, for example ‘Save Changes’ vs. ‘Continue Editing’.</li>
          <li>Buttons are usually positioned at the bottom right of a page or dialog. In a pair, Cancel goes on the left, and Save goes on the right. See the <a href='/patterns/forms/'>form pattern</a> for more info.</li>
        </ul>
      </li>
      <li>Avoid dialogs that are taller than the user’s viewport height. Typical user viewport heights can be as little as 650 pixels. If scrolling is required, the dialog title is pinned at the top, with buttons pinned at the bottom. The dialog itself pins itself to the bottom of the screen. Don’t make the content behind dialogs scrollable.</li>
      <li>To express that the rest of the app is inaccessible, a mask is applied to the rest of the UI when the dialog is open.</li>
      <li>A dialog is usually a surface on top of all other surfaces - they shouldn’t be obscured by other elements or appear partially on screen - avoid placing a dialog on top of another dialog.</li>
      <li>A close icon is shown at the top right of the dialog instead of a traditional ‘Cancel’ button - you don’t need both.</li>
      <li>The title of the dialog, and the label of the primary action focus the user on the dialog’s purpose, and are often exactly the same. For button text, use Initial Capitals, and try to use a verb. A two word pair like ‘Save Invoice’ is ideal.</li>
    </ul>
  </div>
</div>
<div id="dialog-transition" class="dialog-component">
  <div class='dialog-background'></div>
  <div class='dialog'>
    <div class="header-section">
      <h3>Dialog Example</h3>
      <div class='closebox'><i class="icon icon-close dialog-close"></i></div>
    </div>
    <div class="dialog-content">
      <label for="field-1">First Name</label>
      <div class='dls-input-container input-height-m input-width-l'>
        <input type='text' class='dls-input input-small-biz' id='field-1' value=''/>
      </div>
      <label for="field-1">Last Name</label>
      <div class='dls-input-container input-height-m input-width-l'>
        <input type='text' class='dls-input input-small-biz' id='field-1' value=''/>
      </div>
      <label for="field-18">NI Number</label>
      <div class='dls-input-container input-height-m input-width-l'>
        <input type='text' class='dls-input input-small-biz force-focus' id='field-18' placeholder='e.g. QQ123456A'/>
      </div>
      <div class='dls-form-button-container-right'>
        <button class='dls-button button-small small-biz tertiary dialog-close'>Cancel</button><button class='dls-button button-medium small-biz primary dialog-close'>Save</button>
      </div>
    </div>
  </div>
</div>
<div id="dialog-transition" class="fs-dialog-component">
  <div class='dialog-background'></div>
  <div class='dialog fs-dialog'>
    <div class="dls-fs-dialog-header">
    </div>
    <h3>Full Screen Dialog Example</h3>
    <div class='closebox'><i class="icon icon-close fs-dialog-close"></i></div>
    <div class='minimise'><i class="icon icon-minus minimise-fs-dialog"></i></div>
    <div class='expand'><i class="icon icon-fit_width expand-fs-dialog"></i></div>
    <div class='maximise'><i class="icon icon-fit_height minimise-fs-dialog"></i></div>

    <label for="field-1">First Name</label>
    <div class='dls-input-container input-height-m input-width-l'>
      <input type='text' class='dls-input input-small-biz' id='field-1' value=''/>
    </div>

    <label for="field-1">Last Name</label>
    <div class='dls-input-container input-height-m input-width-l'>
      <input type='text' class='dls-input input-small-biz' id='field-1' value=''/>
    </div>
    <label for="field-18">NI Number</label>
    <div class='dls-input-container input-height-m input-width-l'>
      <input type='text' class='dls-input input-small-biz force-focus' id='field-18' placeholder='e.g. QQ123456A'/>
    </div>

    <div class="dls-fs-dialog-footer">
    </div>
    <div class='dls-form-button-container-right dls-fs-dialog-buttons'>
      <button class='dls-button button-small small-biz tertiary fs-dialog-close'>Cancel</button><button class='dls-button button-medium small-biz primary fs-dialog-close'>Save</button>
    </div>
  </div>
</div>
