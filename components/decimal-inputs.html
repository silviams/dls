---
layout: default
secondary_menu_id: decimal-inputs
primary_menu_id: components
title: Decimals, Currency
sketch_file: /downloads/components/text_input.sketch
sketch_file_version: 0.8
summary: Capture currencies or quantities - anything with a decimal point.
tags: input, inputs, form, forms, currency, quantity, decimal, form, forms
---

<p class='summary'>{{ page.summary }}</p>
<div class='content-tab'>
  <ul class='tab-menu'>
    <li class='selected' data-target-tabs='tabs-1'>Design</li>
    <li data-target-tabs='tabs-3'>Guidance</li>
  </ul>
</div>
<div class='secondary-tab visible' id='tabs-1'>
  <div class='container'>
    <p>Align currency values (and other decimals) right, so the decimals line up in a column. Take care to follow local conventions for the formatting of currencies and decimals.</p>
    <div class="pod">
      <table class="pod-table">
        <tr>
          <td>Single Currency Business</td>
          <td>
           <div class='label-input-wrapper'>
            <label for="field-14" class='input-text-align-right'>Total</label>
            <div class='dls-input-container input-height-m input-width-m input-position-right'>
              <input type='text' class='dls-input input-small-biz input-text-align-right' id='field-14' value='1,234.56' />
            </div>
          </td>
        </tr>
        <tr>
          <td>Multi-Currency Business</td>
          <td>
           <div class='label-input-wrapper'>
            <label for="field-14" class='input-text-align-right'>Total (USD $)</label>
            <div class='dls-input-container input-height-m input-width-m input-position-right'>
              <input type='text' class='dls-input input-small-biz input-text-align-right' id='field-14' value='12,345,678.90' />
            </div>
          </td>
        </tr>
        <tr>
          <td>Quantity (Decimal)</td>
          <td>
           <div class='label-input-wrapper'>
            <label for="field-14" class='input-text-align-right'>Quantity</label>
            <div class='dls-input-container input-height-m input-width-s input-position-right'>
              <input type='text' class='dls-input input-small-biz input-text-align-right' id='field-14' value='1.23' />
            </div>
          </td>
        </tr>
        <tr>
          <td>Read-Only</td>
          <td>
           <div class='label-input-wrapper'>
            <label for="field-14" class='input-text-align-right'>Total</label>
            <div class='dls-input-container input-height-m input-width-s input-position-right'>
              <input type='text' class='dls-input input-small-biz input-text-align-right input-read-only' id='field-14' value='1,234.56' disabled/>
            </div>
          </td>
        </tr>
      </table>
      <table class="dls-table">
        <tr><th>Locale</th><th>Full Currency Format</th></tr>
        <tr><td class='right-align'>CA</td><td class='right-align'>$1,234,567.89 CAD</td></tr>
        <tr><td class='right-align'>FR/DE/ES</td><td class='right-align'>€1.234.567,89 EUR</td></tr>
        <tr><td class='right-align'>IE</td><td class='right-align'>€1,234,567.89 EUR</td></tr>
        <tr><td class='right-align'>UK</td><td class='right-align'>£1,234,567.89 GBP</td></tr>
        <tr><td class='right-align'>US</td><td class='right-align'>$1,234,567.89 USD</td></tr>
        <tr><td class='right-align'>ZA</td><td class='right-align'>R1,234,567.89 ZAR</td></tr>
      </table>
      <p>Read more about <a href='https://www.thefinancials.com/Default.aspx?SubSectionID=curformat' target='_blank'>global currency formats</a>.</p>
    </div>
  </div>
</div>
<div class='secondary-tab' id='tabs-3'>
  <div class='container'>
    <h2>Have I got the right component?</h2>
    <ul class='standard-list'>
      <li>Check out the general guidance on <a href='/components/text-inputs/'>inputs</a>, constructing <a href='/patterns/forms/'>forms</a>, and industry guidance from <a href='https://www.lukew.com/resources/web_form_design.asp' target='_blank'>Luke Wroblewski</a>.</li>
      <li>Entering dates? Try a <a href='/components/datepicker/'>Date Picker</a>.</li>
      <li>Entering currencies or quantities? Try a <a href='/components/decimal-inputs/'>Decimal Input</a>.</li>
      <li>Entering numbers without decimals? Try a <a href='/components/number-inputs/'>Number Input</a>.</li>
      <li>Entering data with punctuation inside the field, or where the data is masked? Try a <a href='/components/punctuated-inputs/'>Fieldset</a>.</li>
      <li>Entering longer free text? Try a <a href='/components/textareas/'>Textarea</a>.</li>
      <li>Entering data that's closely related, like parts of an address? Try a <a href='/components/fieldsets/'>Fieldset</a>.</li>
      <li>Searching for something? Try a <a href='/components/dropdowns/'>Type Ahead Dropdown</a> or the Search/Filter configuration for <a href='/components/tables/'>Tables</a>.</li>
      <li>Interested in error states and validation patterns? See the section on <a href='/patterns/errors-validation/'>Validation and Error</a>.</li>
    </ul>
  </div>
  <div class='container'>
    <h2>Which configurations are available?</h2>
    <ul class='standard-list'>
      <li>Height: S; M; L.</li>
      <li>Width: XS; S; M; L; XL; Custom.</li>
      <li>Colour theming by product accent colour.</li>
      <li>Alignment: Right aligned content; Left aligned content.</li>
    </ul>
  </div>
  <div class='container'>
    <h2>Guidance, hints and tips</h2>
    <ul class='standard-list'>
      <li>Align currency values (and other decimals) right, so the decimal places line up in a column.</li>
      <li>If a business only deals with one currency, then consider not showing a currency symbol or ISO currency code. Or, you may choose to only show these on totals, column headers, and field labels, but not for individual line items.</li>
      <li>If a business deals with more than one currency (even if only one currency is shown on a given page), then show both the currency symbol, and ISO currency code, on totals, column headers, and field labels at least, and consider whether there’s a need to show these for individual line items too.
        <ul class='standard-list'>
          <li>Always show both the currency symbol and ISO code, because in some situations, the symbol may be the same for two currencies, like USD $, and CAD $.</li>
          <li>Consider using the field label format "Total (USD $)"</li>
          <li>If a field is permanently read-only (like a total value), display it as formatted text, without a field border.</li>
          <li>Flexible input: Allow users to enter currencies with, and without thousands punctuation.</li>
        </ul>
      </li>
    </ul>
  </div>
</div>
<div class="system-message system-toast success-message copied-to-clipboard">
  <div class='system-message-body'>Copied to clipboard</div>
</div>
