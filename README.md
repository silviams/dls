# Design System Portal

This repository is a Jekyll site used to provide the Sage Design Language system through a browser based portal.

## Running the site locally

1. Clone the dls repository (`git clone git@github.com:Sage/dls.git`)
2. `cd dls`
3. `bundle install`
4. `bundle exec jekyll serve`
5. Navigate to [http://localhost:4000/](http://localhost:4000/) in your favourite browser

## Contributing to the repo

Please ensure the following checklist has been completed

- Release notes added
- Visuals before/after
