---
layout: default
secondary_menu_id: content-advice
primary_menu_id: content
title: Advice for Writing
---

<p class="summary">When creating content these general guidelines will help ensure your writing hits the mark.</p>

<h3>Keep your writing simple and straightforward</h3>

<p>You don’t read one word at a time, you anticipate words and fill them in as you bounce around the text.. As a writer it's your job to make this as easy as posible for the reader.</p>

<p>To do this:</p>

 <ul class="standard-list">
  <li>Write for the active voice, not the passive.</li>
  <li>Favour simple tenses.</li>
  <li>Be precise and definitive - Always be clear about who's doing what and take responsibility.</li>
  <li>Be inclusive and accessible.</li>
</ul>

<h3>Make content digestible and skimmable</h3>

<p>Readability and legibility is crucial when it comes to text. Keep length within an optimal range of between 50-75 characters. If your lines are too wide and a reader’s eyes will have issues focussing, too short and the eye will have to travel back and forward too often, breaking the natural rhythm of reading. Ensure the text also has sufficient room to breathe with suitable line spacing.</p>

<p>To do this:</p>

<ul class="standard-list">
  <li>Be mindful of how many words are able to fit on a free line and how the scan and meter of sentences is affected. Amend as appropriate and consider the use of hard line breaks to insert notional pauses where they make sense to users.</li>
    <li>Avoid extending text onto multiple lines or using too many labels in close proximity.</li>
  <li>Make it brief and to the point. Avoid the obvious. Avoid padding, Avoid redundant prepositional phrases. Avoid verbosity. Avoid pomposity. Avoid idioms.</li>
  <li>Use words that are easy to understand.</li>
  <li>Break up text into subheaded sections - Don’t use full-stops in headings/lines. (Do use at the end of a first sentence if there’s a second sentence). In more conversational text, it can make sense to use more conversational headings and subheadings - it makes the text more approachable and helps understanding when introducing new and potentially complex concepts.  In less conversational settings, explore the potential to shorten and formalise headings.</li>
  <li>Use bullet lists.</li>
  <li>'Front-load' subheadings, titles and bullet points to put the most important information first - use white space, line breaks, bolding, bullets and headings.
  <li>Use lower case where possible (Capital letters are reputed to be up to 18% harder for users to read).</li>
  <li>Cut out common introductory phrases (Such as Would you like to... Are you sure you want to...) in favour of brevity (For example, Delete Invoice? not Would you like to delete this invoice?).</li>
  <li>Cut pleasantries like 'Please' unless the user is being particularly inconvenienced and the use of politeness might modify bad feeling.</li>
  <li>Reduce cognitive load at every possible step. Always look to shorten, simplify and remove.</li>
  <li>Avoid jargon, keep things simple, don't assume knowledge (ensure the user has access to the information they need - out of flow, if disruptive) for beginners and experts alike.</li>
  <li>The user should always be clear as to the effect of an action without needing to pause and decipher every other word.</li>
  <li>Don't over communicate - using too much information.</li>
  <li>Don't be overly dramatic. This can cause fear, panic and confusion where there doesn't need to be. Think about what the user needs and will think at any given point in the flow.</li>
  <li>Editing or reducing for consistency is important. Favour brevity for the easy absorption of information. Ensure all key information remains and isn't truncated or omitted. Remove information or background that isn't required in context.</li>
  <li>Don't sacrifice meaning for brevity. Always ensure there is sufficient meaning for the context, being presented.</li>
  <li>Favour specific values not vague generalities.</li>
  <li>Never use a foreign phrase, scientific word or a jargon word if you can think of an everyday English equivalent.</li>
</ul>

<h3>Adopt a conversational tone</h3>
<p>We don’t want to come across cold as a monolithic organisation. We should, therefore, use the kind of language you’d use if you were talking to the person you’re writing for and avoid business speak and vagaries.</p>

<p>To do this:</p>

<ul class="standard-list">
  <li>Be human and talk like one.</li>
  <li>Don’t use internal messaging (Such as technical messaging intended for and only decipherable by programmers, project terms or feature names). Always replace with human, plainly written equivalents.</li>
  <li>Understand how actions are likely to make users feel and empathise, altering messaging to suit.</li>
  <li>Use contractions and linking words (such as And, So, But and Because).</li>
  <li>Avoid jargon where possible.</li>
  <li>Don’t blame the user - Don’t point the finger and be accusatory. Focus instead on what this means (what’s happened and its impact) and how to rectify (do this [description] to get the result you want) in a way that makes sense for the user (given the individual’s understanding and the preceding context).</li>
</ul>

<h3>Use imagery appropriately</h3>

<p>Always consider what visuals could add to words - Putting pictures next to words helps your reader understand what the words mean.</p>

<p>To do this:</p>

<ul class="standard-list">
  <li>Use only images that support and help describe the content (Do not use imagery for imagery’s sake).</li>
  <li>Use the same image to say the same thing in every identical context.</li>
  <li>Keep imagery simple and easy to decipher.</li>
  <li>Never rely on an image to convey entire meaning - always summarise content in text.</li>
</ul>
