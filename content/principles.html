---
layout: default
secondary_menu_id: content-principles
primary_menu_id: content
title: Content Principles
---

<p class="summary">Our principles give us purpose and clarity when designing Sage Business Cloud experiences. Here we show how these are balanced to generate great content.</p>

<h3>Our principles</h3>

<p>All content should be:</p>

<ul class="standard-list">
  <li><strong>Clear</strong></li>
</ul>

<p>Content should be unambiguous and jargon-free, and be easily understood in context.</p>
<ul class="standard-list">
  <li><strong>Concise</strong></li>
</ul>

<p>Content should be economical, action-focussed with what's important front-loaded.</p>

<ul class="standard-list">
  <li><strong>Useful</strong></li>
</ul>

<p>Content should be informative, and appropriate, guiding and directing next actions.</p>

<h3>Getting the balance right when applying principles</h3>

<p>These principles are not always in harmony, rather they compete with each other and must be balanced in a way so as to provide the considered best result in a particular context.</p>

<p>Consider this example of a message displayed in product. Applying the content creation principles you can see how the message can be honed to provide the best overall result:</p>

<table class='dls-table'>
  <tbody>
    <tr>
      <td>
        <p><strong>Original</strong></p>
      </td>
      <td>
        <p><strong>Failure</strong><strong><br /></strong><strong>An authentication error has occurred</strong><strong><br /></strong><strong>OK</strong></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><strong>Clear</strong></p>
      </td>
      <td>
        <p><strong>Sign-in error</strong><strong><br /></strong><strong>You entered an incorrect password</strong><strong><br /></strong><strong>OK</strong></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><strong>Clear, Concise</strong></p>
      </td>
      <td>
        <p><strong>Wrong password</strong><strong><br /></strong><strong>OK</strong></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><strong>Clear, Concise, Useful</strong></p>
      </td>
      <td>
        <p><strong>Wrong password</strong><strong><br /></strong><strong>TRY AGAIN / RECOVER PASSWORD</strong></p>
      </td>
    </tr>
  </tbody>
</table>

<h3>What this balancing looks like in practice</h3>

<p>When thinking about how we can achieve balance we can draw inspiration from our design principles. These must also be balanced to get the best result:</p>

<table class='dls-table'>
  <tbody>
    <tr>
      <td>&nbsp;</td>
      <td>
        <p><strong>Example content design considerations</strong></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><strong>1 - Be clear, and guide.</strong></p>
      </td>
      <td>
        <p><strong>By being unambiguous, purposeful and helpful we create an environment where customers can do what they need to do efficiently and effectively. We act as ‘a friend over the shoulder’ guiding our customers through new tasks and experiences in appropriate detail and reminding where appropriate.</strong></p>
        <p>&nbsp;</p>
        <p><strong><br /></strong><span style="font-weight: 400;">Are our labels using terms easily understood by our audience?</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Are we using labels consistently? </span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Do we present relevant &lsquo;next steps&rsquo; for users when they have finished a task?</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Do we present complex set-up in an easy-to-understand, non-intimidating and easy-to-configure fashion?</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Do we provide additional help or context for first-time attempts at tasks?</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Do we make technical terms and concepts as clear as we possibly can?</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Do we focus on the payoff? Tasks can feel arduous, we should keep users motivated.</span></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><strong>2 - Reassure, and be seen as trustworthy.</strong></p>
      </td>
      <td>
        <p><strong>Customers should feel supported in everything do, confident that we’re on their side and won’t let them down. They can rely on us.</strong></p>
        <p>&nbsp;</p>
        <p><span style="font-weight: 400;">Are the effects of actions communicated effectively?</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Do we explain what is happening/has happened in language that&rsquo;s easy to understand?</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Do we address common misunderstandings or fears in task flows?</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Does our language inspire confidence and trust in the Sage Business Cloud platform?</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Do we flag potential destructive actions and unintended effects appropriately?</span></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><strong>3 - Be adaptive, and contextual.</strong></p>
      </td>
      <td>
        <p><strong>Our experiences will be exceptionally engineered and deliver efficiently and effectively at a pace that suits our customers, who they are, and what they need to do. Relevant data and content is placed front and centre of the experience with supporting tools and guidance.</strong></p>
        <p>&nbsp;</p>
        <p><span style="font-weight: 400;">Do we make intelligent suggestions based on what we know about the user and/or what they have done previously?</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Do we provide flexibility to customise information and results generated, with particular tasks/results in mind?</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Do we provide in-context help that&rsquo;s relevant to beginners and experts at appropriate points?</span></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><strong>4 - Be useful, and delightful. </strong></p>
      </td>
      <td>
        <p><strong>Our experiences should be pleasurable - precision-engineered to meet customer needs efficiently and effectively - and sprinkle delight where appropriate.</strong></p>
        <p>&nbsp;</p>
        <p><span style="font-weight: 400;">Do all labels and functions make sense in the context of a task flow?</span></p>
        <p><span style="font-weight: 400;">Do we reward and enthuse as our users grow in confidence and use more of our products - moving from empty state to high adoption?</span></p>
      </td>
    </tr>
  </tbody>
</table>

<h3>In summary...</h3>

<p>Our content creation principles can help us decide what we want to say, in how much detail, and when to say it.</p>

<p>They can also help inform the words and phrases we choose to use in particular contexts. Balancing principles in context is required to get the best result.</p>

<p>They can also help us consider when visual design can convey meaning and when words should be used instead of, or alongside, imagery and other design elements.</p>
