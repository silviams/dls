---
layout: default
secondary_menu_id: content-types
primary_menu_id: content
title: Writing Types of Content
---

<p class="summary">This page contains guidance on writing the range of content types that a user may encounter in product interface.</p>

<h2>Messages</h2>
<p>In general, messages should be:</p>

<ul class="standard-list">
  <li>Timely not noisy.</li>
  <li>Informative not verbose.</li>
  <li>Actionable not static.</li>
</ul>

<h3>Messages by content type</h3>

<p>Select a content type for advice on writing this kind of messaging:</p>
<p><a href="#1">Alerts and warnings (About to happen / Happened)</a></p>
<p><a href="#2">Empty states</a></p>
<p><a href="#3">Errors</a></p>
<p><a href="#4">Confirmations (Completion / Success)</a></p>
<p><a href="#5">Information Messages</a></p>
<p><a href="#6">System Messages</a></p>
<p><a href="#7">First time use</a></p>
<p><a href="#8">Help</a></p>
<p><a href="#9">Interrupts</a></p>
<p><a href="#10">Discovery / Promotion</a></p>
<p><a href="#11">Feedback</a></p>
<p><a href="#12">What's new</a></p>
<p><a href="#13">Labels</a></p>
<p><a href="#14">Interrupt on closure</a></p>
<p><a href="#15">Progress messages</a></p>
<p><a href="#16">Guidance / walkthrough</a></p>
<p><a href="#17">Release Notes</a></p>
<p><a href="#18">Emoji</a></p>

<!-- ALERTS AND WARNINGS (PRIOR TO CONFIRMATION) -->
<a name="1"></a>
<h2>Alerts and Warnings (About to happen / Happened)</h2>
<p><strong>How these might be presented:</strong></p>

<ul class="standard-list">
  <li>Dialog.</li>
  <li>Icon and/or text next to information or function.</li>
  <li>Flash.</li>
</ul>

<p><strong>What the user might feel:</strong> Concerned, surprised, alarmed, concerned.</p>
<p><strong>What we want the user to feel:</strong> Informed, understood, calm, reassured.</p>
<p><strong>What these are for:</strong></p>

<ul class="standard-list">
  <li>For making the user aware of a change, or an effect of an action being carried out.</li>
  <li>Used when there’s a risk of loss (Not as a step along a continuation or flow).</li>
  <li>Where the effect is potentially wide-ranging, damaging or catastrophic we need to be sure the user is aware of the effect (and ideally has chance to back out, or reverse these effects).</li>
</ul>

<p><strong>How to write these:</strong></p>

<ul class="standard-list">
  <li>Always put the most important information upfront.</li>
  <li>The user should be made aware of impacts using a proportionate (not alarmist) tone that is applied consistently with a sliding scale used to ensure parity of like situations.</li>
  <li>Always ensure that there’s enough information given to understand the potential impact so the user can make a fully informed choice.</li>
  <li>One paragraph, one subject. In general, use paragraphs to split up the information.</li>
  <li>Avoid the pitfalls of punctuation by restricting yourself as far as-is possible to full-stops and commas.</li>
  <li>Confirmation messages should be clear and unambiguous. They should leave the user in no doubt as to what has happened and the outcome (particularly if different or not the complete request). Show what happens when you successfully complete the flow.</li>
  <li>Messages should always be in keeping with the broader task flow (signpost next steps, link to related resources etc.). Don’t swamp the user, but do think about what’s useful and what’s next.</li>
  <li>Always allow a close / exit route from the confirmation.</li>
  <li>Whenever a question is posed, you should provide all the information required to allow the user to provide an informed answer. The risk of any unintended action should always be made clear.</li>
  <li>Only use confirmations where necessary (if there is a solid reason not to proceed, in this case make the reason obvious and provide all the information needed to allow the user to make a good decision).</li>
  <li>Remove unnecessary confirmations.</li>
</ul>

<!-- EMPTY STATES -->
<a name="2"></a>
<h2>Empty states</h2>
<p><strong>Sub-types:</strong></p>

<ul class="standard-list">
  <li>Nothing to see.</li>
  <li>Here's what to do...</li>
  <li>Get started by...</li>
  <li>Roadblock (Item not available, Geo-restriction).</li>
  <li>New - Exploration or promotion of a new feature.</li>
  <li>Upgrade / Unlock - Restricted access (How to unlock is prominent / make benefits of doing so, obvious).</li>
</ul>

<p><strong>How this might be presented:</strong></p>

<ul class="standard-list">
  <li>In the place where the populated material would appear.</li>
  <li>As an overlay or tooltip where the populated material would appear (or near a preceding action).</li>
  <li>As text, animation, illustration, video...</li>
</ul>

<p><strong>What these are for:</strong></p>

<ul class="standard-list">
  <li>Show that the state is intended (visually and/or with text).</li>
  <li>Show how a user can get started with a feature.</li>
</ul>

<p><strong>How to write these:</strong></p>

<ul class="standard-list">
  <li>Support and encourage users to engage with the feature by using the space to enthuse/promote the feature/benefit.</li>
  <li>An empty state can be eye-catching and delightful. There's the potential to be humorous/light-hearted to attract interest and attention.</li>
  <li>Show the user how to get going. Describe how users can get involved using the feature and what the benefits are.</li>
  <li>Show how these states become populated / are used (animation / walkthrough).</li>
  <li>Use the state as a marketing ‘sell’, encouraging deeper use / integration if appropriate so long as there's a clear call to action.</li>
  <li>Always offer users the chance to dismiss / re-engage any states on their own terms - The user should be able to jump in and out as required.</li>
  <li>Always differentiate the state from the populated state and make it clear where the interaction is triggered from, with a clear call to action.</li>
  <li>The user should be given the opportunity to ignore or pass on the functionality at this time. In other words, the user should be able to return to other areas of functionality (ie. Don't lock a user into the flow).</li>
  <li>When a user makes an error you should explain what's wrong and how to overcome or resolve the problem (ie. Don't leave a user stranded with nowhere to go).</li>
</ul>

<p>See also: Examples of <a href="http://emptystat.es" target="_blank">empty states</a> on the emptystates.es website.</p>

<!-- ERRORS -->
<a name="3"></a>
<h2>Errors</h2>
<p><strong>Sub-types:</strong></p>

<ul class="standard-list">
  <li>Data validation error.</li>
  <li>Account error.</li>
  <li>System error.</li>
</ul>

<p><strong>What the user might feel:</strong> Annoyed, Interrupted, Frustrated, Unsure.</p>

<p><strong>What these are for:</strong></p>

<ul class="standard-list">
  <li>To let the customer know that something has gone wrong and (if possible) how to rectify it. The aim should be to provide a solution as quickly as possible and some understanding of what's gone wrong and why.</li>
</ul>

<p><strong>How to write these:</strong></p>

<ul class="standard-list">
  <li>Favour brevity and specificity, wherever possible.</li>
  <li>When describing errors, show what fell through, and why. Describe what to do to resolve them.</li>
  <li>Where it's not possible to show a reason and/or a solution, don't. Don't bluster and be vague. Be specific whenever possible.</li>
  <li>Use language in line with similar scenarios in other products or scenarios. (There should be no conflict between a generally understood definition of a word and the way we're using it in the error message.)</li>
  <li>When describing an error we need to make sure that the user is left in no doubt as to the cause and any remedial steps they can take to rectify it.</li>
  <li>Good error messages have a problem, a cause, a solution. They should be relevant, actionable, user-centred, brief, clear, specific, courteous and rare.</li>
  <li>Avoid error messages where the user may be under the impression that task has already been dealt with (deleted, cancelled). Return reasonable results (not errors), wherever possible.</li>
  <li>If an unknown error can't be suppressed it's better to be upfront about the lack of information. Provide specific, actionable information (if it's likely to be helpful), ideally.</li>
</ul>

<!-- CONFIRMATIONS (COMPLETION/SUCCESS) -->
<a name="4"></a>
<h2>Confirmations (Completion / Success)</h2>
<p><strong>Sub-types:</strong></p>

<ul class="standard-list">
  <li>Workflow completed.</li>
  <li>Transaction completed.</li>
  <li>Data received.</li>
  <li>Information added.</li>
  <li>Action performed.</li>
</ul>

<p><strong>What these are for:</strong></p>
<p>To let the customer know that a process or transaction has been completed successfully. Recaps the activity and confirms the success.</p>
<p>How to write these:</p>

<ul class="standard-list">
  <li>Get to the point.</li>
  <li>Reflect language and concepts already established (Parity / consistency).</li>
  <li>Recap the action performed if it aids clarity and understanding.</li>
</ul>

<!-- INFORMATION MESSAGES-->
<a name="5"></a>
<h2><strong>Information Messages</strong></h2>
<p><strong>Sub-types:</strong></p>

<ul class="standard-list">
  <li>Examples</li>
  <li>Explanations</li>
  <li>Hints</li>
  <li>Advisory</li>
  <li>Contextual</li>
</ul>

<p><strong>What these are for:</strong></p>
<p>Provide additional related details that will set context, aid understanding, or otherwise help customers.</p>
<p>How to write these:</p>

<ul class="standard-list">
  <li>Favour brevity</li>
  <li>Get to the point.</li>
  <li>Front-load information.</li>
  <li>Show only relevant information and options</li>
</ul>

<!-- SYSTEM MESSAGES -->
<a name="6"></a>
<h2>System Messages</h2>
<p><strong>Sub-types:</strong></p>

<ul class="standard-list">
  <li>Site maintenance / restricted performance</li>
  <li>Browser issues</li>
  <li>Progress indicators</li>
  <li>Page not found</li>
  <li>Server issues</li>
</ul>

<p><strong>What these are for:</strong></p>
<p>Lets the customer know about relevant issue(s) within the application environment. Provides contextual information about the application, browser, task progress, server issues, including scheduled maintenance or unplanned downtime.</p>
<p><strong>How to write these:</strong></p>

<ul class="standard-list">
  <li>Use non-system-orientated language that is easily understood.</li>
  <li>Avoid technical nuance.</li>
  <li>Always focus on effect / activity that's just been undertaken.</li>
  <li>Show like messages in a similar place in the interface so the user associates this placement with similar content and meaning.</li>
</ul>

<!-- FIRST TIME USE -->
<a name="7"></a>
<h2>First-time Use</h2>
<p><strong>Sub-types:</strong></p>

<ul class="standard-list">
  <li>Empty states (Either showing how to populate or show and tell the population flow).</li>
  <li>Guided tour.</li>
  <li>Set-up / configuration (Wizard).</li>
</ul>

<p><strong>What these are for:</strong></p>

<ul class="standard-list">
  <li>Guiding the customer from the beginning of a (relatively) new experience (be it a new product or feature, entering data, or encountering a new screen) and 'sells' the benefits of utilising and engaging with the new experience.</li>
</ul>

<p><strong>How to write these:</strong></p>
<ul class="standard-list">
  <li>Point out relevant benefits and signpost features or first steps.</li>
  <li>Be concise and precise.</li>
  <li>Highlight ways to find out more (where these are available).</li>
  <li>Always try and explain briefly and name consistently so you don't have to constantly signpost marketing or support materials relevant to the context.</li>
</ul>

<!-- HELP -->
<a name="8"></a>
<h2>Help</h2>
<p><support>Sub-types</support></p>

<ul class="standard-list">
  <li>Support knowledgebase</li>
  <li>Link to help article from 'i' icon.</li>
  <li>Dialogue presented when the user struggles.</li>
  <li>Just-in time straps or tooltips.</li>
</ul>

<p><strong>What these are for:</strong></p>

<ul class="standard-list">
  <li>Anticipates customer questions and provides guidance and reassurance at the point of need.</li>
  <li>Promoting the best method of obtaining help in any given context.</li>
</ul>

<p><strong>How to write these:</strong></p>
  <ul class="standard-list">
  <li>Break down actions / explanation into a logical step-by-step flow geared toward what the user is likely to be thinking or feeling.</li>
  <li>Place the user into the context of the broader task / flow (Cutting down on granular articles focussed on micro-tasks).</li>
</ul>

<!-- INTERRUPTS -->
<a name="9"></a>
<h2>Interrupts</h2>
<p><strong>Sub-types:</strong></p>

<ul class="standard-list">
  <li>Full-screen.</li>
  <li>Dialogs.</li>
  <li>Strap.</li>
</ul>

<p><strong>What these are for:</strong></p>

<ul class="standard-list">
  <li>When a pause is required in a flow.</li>
  <li>To encourage the user to carefully think about an action or to understand an effect.</li>
</ul>

<p><strong>How to write these:</strong></p>

<ul class="standard-list">
  <li>Aim for clarity and brevity.</li>
  <li>Make effects clear.</li>
  <li>Make notifications prominent but easily dismissible to allow a quick return to what the user was (or was about to) do. </li>
  <li>Present information, relevant of context.</li>
</ul>

<p><strong>How to write these:</strong></p>

<ul class="standard-list">
  <li>Get to the point.</li>
  <li>Front-load the important information.</li>
  <li>Provide an easy way to perform / navigate to an area if an action is required to be undertaken.</li>
</ul>

<!-- DISCOVERY/PROMOTION -->
<a name="10"></a>
<h2>Discovery / Promotion</h2>
<p><strong>Sub-types:</strong></p>

<ul class="standard-list">
  <li>Embedded text.</li>
  <li>Inline messages.</li>
  <li>Pop-up messages.</li>
  <li>Overlays.</li>
</ul>

<p><strong>What these are for:</strong></p>

<ul class="standard-list">
  <li>Tell users about new features and services.</li>
  <li>Tell users how to find (and/or understand the benefits of existing ones.</li>
  <li>Show users how to do something by providing contextual information.</li>
</ul>

<p><strong>How to write these:</strong></p>

<ul class="standard-list">
  <li>Don't get in the way of what the user is trying to do unless it's necessary to prevent unintended or impactful events.</li>
  <li>Use sparingly as this method of displaying information is quite disruptive.</li>
  <li>Use clear instructions.</li>
  <li>Be enthusiastic, focus on benefits rather than features.</li>
  <li>Make it clear how users can trigger the resulting process (start the feature flow).</li>
  <li>Guide users towards suitable options based on what they're doing, or suggesting features you know they haven't tried.</li>
  <li>Offer a personalised journey / promotion based on what the user has or hasn't done or tried thus far at the point the screen is presented.</li>
</ul>

<!-- FEEDBACK -->
<a name="11"></a>
<h2>Feedback</h2>
<p><strong>Sub-types:</strong></p>

<ul class="standard-list">
  <li>App ratings.</li>
  <li>Surveys.</li>
  <li>Feedback fields.</li>
</ul>

<p><strong>What these are for</strong></p>

<ul class="standard-list">
  <li>Allowing users to feel heard by providing appropriate mechanisms for users' feedback.</li>
  <li>Providing a feedback mechanism in the moment of a particular task or in the general product environment.</li>
  <li>Providing a mechanism to find out how individual and/or collective feedback is influencing product.</li>
  <li>Customer engagement.</li>
  <li>Encouraging participation to refine and improve the experience by making people's voices heard.</li>
</ul>

<p><strong>How to write these:</strong></p>
<ul class="standard-list">
  <li>Encourage and value people's contributions.</li>
  <li>Show what the contribution is used for (for example, making the product better, improving a specific feature).</li>
  <li>Reassure that getting involved won't take too much time or effort.</li>
  <li>Break down questions or options into logical, grouped steps.</li>
  <li>Ensure language is easily understood or comprehended.</li>
  <li>Acknowledge submission of feedback and confirm any next steps - response times, how to chase up (if necessary).</li>
</ul>

<!-- WHAT"S NEW -->
<a name="12"></a>
<h2>What's new</h2>
<p><strong>Sub-types:</strong></p>

<ul class="standard-list">
  <li>App store.</li>
  <li>Release notes (App Store / Product Blog).</li>
  <li>In-product.</li>
  <li>Email.</li>
</ul>

<p><strong>What these are for:</strong></p>
<ul class="standard-list">
  <li>To make the customer aware of and/or understand the benefits of features and functionality and understand what these features or functionality mean for them.</li>
  <li>To provide a human voice that communicates and reassures users as to the active development of the product.</li>
</ul>

<p><strong>How we write these:</strong></p>
<ul class="standard-list">
  <li>Always focus on what’s in it (what the benefit is) for the customer.</li>
    <li>Use language that informs, entices and motivates.</li>
    <li>Use short scannable sentences and bullet points to highlight the essentials.</li>
    <li>Focus on what’s important to the user (ie. not everything, not business priorities).</li>
    <li>Where details are sketchy, try and them provide users with some (even limited) context - (eg. Fixed some pesky bugs).</li>
  </ul>

<!-- LABELS -->
<a name="13"></a>
<h2>Labels</h2>
<p><strong>What these are for:</strong></p>
<ul class="standard-list">
  <li>Navigation of features and data.</li>
</ul>

<p><strong>How to write these:</strong></p>
<ul class="standard-list">
  <li>Keep labels brief and consistent.</li>
  <li>When using an icon, ideally use text as well.</li>
  <li>Be mindful of any convention in other software (eg. Commonality of terms like Open, File etc.)</li>
  <li>Maintain expected behaviour(s) on selecting a label with software and in the wider environment</li>
  <li>Use action-focussed language - Start, Create, Send etc.</li>
  <li>Be descriptive and use the words and phrases users themselves use.</li>
  <li>Don't call multiple things the same (or similar) things.</li>
  <li>Use simple language, easy for scanning or skim-reading.</li>
  <li>Use commonly-known words and concepts to describe function.</li>
  <li>Labels should be short, scannable, mindful of convention, action focussed.</li>
  <li>Labels should be brief.</li>
  <li>Try and avoid a design featuring a multitude of labels, which would require processing a large number of bite-sized labels. Wherever possible reduce the readers' cognitive load from processing a screen-full of bite-sized information.</li>
  <li>Label appropriately, mindful of a users' journey in and out of the information - what is known, what has been presented (and how) previously.</li>
</ul>

<p><strong>Examples:</strong> Print | Email | Create | New Invoice |</p>

<!-- INTERRUPT ON CLOSURE -->
<a name="14"></a>
<h2>Interrupt on Closure</h2>
<p>What these are for:</p>

<ul class="standard-list">
  <li>Ensuring a user has considered something that may cause a big impact if they exit the program at this time.</li>
</ul>

<p><strong>How to write these:</strong></p>
<ul class="standard-list">
  <li>Use sparingly.</li>
  <li>Get to the point.</li>
  <li>Include a clear call to action.</li>
  <li>Clearly signpost and ability to clear with (preferably) automatic dismissal of messaging.</li>
</ul>

<!-- PROGRESS MESSAGES -->
<a name="15"></a>
<h2>Progress messages</h2>
<p><strong>Sub-elements:</strong></p>
<ul class="standard-list">
  <li>Starting.</li>
  <li>Progressing.</li>
  <li>Ending.</li>
</ul>

<p><strong>What these are for:</strong></p>

<ul class="standard-list">
  <li>Communicating a place in a series of actions.</li>
  <li>Communicating the progress of an action or actions to manage expectations.</li>
</ul>

<p><strong>How to write these:</strong></p>

<ul class="standard-list">
  <li>Use consistent and meaningful scales.</li>
  <li>Allow for cancel/dismissal.</li>
  <li>Allow for undo (at the end of task), if at all possible.</li>
</ul>

<!-- GUIDANCE / WALKTHROUGHS -->
<a name="16"></a>
<h2>Guidance / walkthrough</h2>
<p><strong>What these are for:</strong></p>

<ul class="standard-list">
  <li>Splitting a complex process into a more manageable series of logical steps.</li>
    <li>Exploring lots of features/options to allow users to become familiar with featureset.</li>
</ul>

<p><strong>How to write these:</strong></p>

<ul class="standard-list">
  <li>Show and tell - help users find what they want in time, and in context.</li>
  <li>Acknowledge pain points and challenges and assure users we're here to help with appropriate signposts, workarounds, clarity (so as to understand choices).</li>
</ul>

<!-- RELEASE NOTES -->
<a name="17"></a>
<h2>Release Notes</h2>
<p><strong>What these are for:</strong></p>

<ul class="standard-list">
  <li>Informing users of fixes and new features.</li>
  <li>Helping users see that our products are actively evolving and developing to meet needs better, and meet new and emerging needs.</li>
</ul>

<p><strong>How to write these:</strong><p>

<ul class="standard-list">
  <li>Get to the point.</li>
  <li>Focus on what the changes mean (or the errors were meaning) to customers (rather than to us, or the brand, or the product family).</li>
  <li>It is better to be vague rather than specific if the message doesn't add much to aid user understanding or requires too much explaining in order by understood.</li>
  <li>Focus on benefits not features. Show the benefits of the fix rather than describing the fix itself.</li>
</ul>

<!-- EMOJI-->
<a name="18"></a>
<h2>Emoji</h2>
<p><strong>What these are for:</strong></p>

<ul class="standard-list">
  <li>Generally apply emoji sparingly as sprinkles of delight. Never replace words used to convey meaning with emoji - always augment.</li>
</ul>

<p><strong>How we write these:</strong></p>

<ul class="standard-list">
  <li>Alongside text, given appropriate context and sufficient understanding of the emoji-set being used.</li>
  <li>What the user will respond in terms of an enthusiastic, empathetic or otherwise emotional response. The label should reflect (though not necessarily directly translate) the meaning of the emoji.</li>
  <li>Take time to select emojis that are easily understood by most.</li>
</ul>
