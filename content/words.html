---
layout: default
secondary_menu_id: content-words
primary_menu_id: content
title: Words
---

<p class="summary">The words we choose have an enormous impact on how we are portreyed and received. Here we show how best to apply words to communicate with clarity and purpose.</p>

<h2>Choosing the words we use</h2>

<p>We don’t want to come across cold, as a monolithic and unfriendly organisation.</p>

<p>General advice would be to use exactly the kind of words and phrases you’d use if talking to the person you’re writing for. We should avoid business speak and vagaries and favour clear, conversational, natural-sounding communication.</p>

<h3>Plain English</h3>

<p>Always strive for clarity, and use easily understood words and phrases allowing easy, unimpeded access to what is being communicated.</p>

<p>To make communication as digestible as possible, strive for a simple word order - Subject, auxiliary verb, main verb, object.</p>

<h3>Use of technical and discipline-specific language</h3>

<ul class="standard-list">
  <li>Use plain words where possible but if you need to use technical terms, explain them, where appropriate. This may not always be in the context of the immediate flow but from messaging before or after.</li>
  <li>Use technical terms, abbreviations or acronyms sparingly to avoid communications becoming impenetrable.</li>
  <li>If you know that a user is new to a task you should attempt to guide and explain as much as possible. Similarly, if you know what a user has (or hasn't done) you should use this information to provide appropriate levels of contextual support.</li>
  <li>Don’t reinvent phrases for common or discipline specific concepts. It's unclear and confusing, as the user will likely encounter the more common phrases anyway.</li>
</ul>

<h3>Words to avoid</h3>

<p>Unnecessarily fussy words should be avoided and clearer alternatives used instead. These include but aren’t limited to:</p>

<table class="dls-table">
  <tbody>
    <tr>
      <td>
        <p><strong>DON&rsquo;T</strong></p>
      </td>
      <td>
        <p><strong>DO</strong></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><span style="font-weight: 400;">Assistance</span></p>
      </td>
      <td>
        <p><span style="font-weight: 400;">Help</span></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><span style="font-weight: 400;">Commence</span></p>
      </td>
      <td>
        <p><span style="font-weight: 400;">Start</span></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><span style="font-weight: 400;">Enable</span></p>
      </td>
      <td>
        <p><span style="font-weight: 400;">Let</span></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><span style="font-weight: 400;">Ensure</span></p>
      </td>
      <td>
        <p><span style="font-weight: 400;">Make sure</span></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><span style="font-weight: 400;">Further </span></p>
      </td>
      <td>
        <p><span style="font-weight: 400;">More</span></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><span style="font-weight: 400;">However</span></p>
      </td>
      <td>
        <p><span style="font-weight: 400;">But</span></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><span style="font-weight: 400;">In order to</span></p>
      </td>
      <td>
        <p><span style="font-weight: 400;">To</span></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><span style="font-weight: 400;">Obtain</span></p>
      </td>
      <td>
        <p><span style="font-weight: 400;">Get</span></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><span style="font-weight: 400;">Provide</span></p>
      </td>
      <td>
        <p><span style="font-weight: 400;">Give</span></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><span style="font-weight: 400;">Query</span></p>
      </td>
      <td>
        <p><span style="font-weight: 400;">Question</span></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><span style="font-weight: 400;">Request</span></p>
      </td>
      <td>
        <p><span style="font-weight: 400;">Ask or Get</span></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><span style="font-weight: 400;">Require</span></p>
      </td>
      <td>
        <p><span style="font-weight: 400;">Need</span></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><span style="font-weight: 400;">Resolve</span></p>
      </td>
      <td>
        <p><span style="font-weight: 400;">Fix</span></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><span style="font-weight: 400;">Therefore</span></p>
      </td>
      <td>
        <p><span style="font-weight: 400;">So</span></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><span style="font-weight: 400;">Utilise</span></p>
      </td>
      <td>
        <p><span style="font-weight: 400;">Use</span></p>
      </td>
    </tr>
  </tbody>
</table>

<p>&nbsp;</p>

<p>The GOV.UK <a href="https://www.gov.uk/guidance/style-guide/a-to-z-of-gov-uk-style#words-to-avoid" target="_blank">list of words to avoid</a> is a useful  guide when it comes to avoiding overly stuffy terms.</p>

<p>It's not just fussy words that need to be considered. Always consider whether there’s possibility of truncating what’s being said to get a better (more easily understood) result.</p>

<p>Reading text out loud is the best way to check suitability and make sure that words are conversational and clear.</p>

<table class="dls-table">
  <tbody>
    <tr>
      <td>
        <p><strong>Longer version</strong></p>
      </td>
      <td>
        <p><strong>Shorter version</strong></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><span style="font-weight: 400;">On a regular basis</span></p>
      </td>
      <td>
        <p><span style="font-weight: 400;">Regularly</span></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><span style="font-weight: 400;">If at all possible </span></p>
      </td>
      <td>
        <p><span style="font-weight: 400;">If possible</span></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><span style="font-weight: 400;">During the month of November</span></p>
      </td>
      <td>
        <p><span style="font-weight: 400;">In November</span></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><span style="font-weight: 400;">On two separate occasions</span></p>
      </td>
      <td>
        <p><span style="font-weight: 400;">Twice</span></p>
      </td>
    </tr>
  </tbody>
</table>

<h3>Describing interactions with UI</h3>

<p>Customers interact with products using different input methods: keyboard, mouse, touch, voice, and more. So use generic verbs that work with any input method.</p>

<p>The <a href="https://docs.microsoft.com/en-us/style-guide/procedures-instructions/describing-interactions-with-ui" target="_blank">Microsoft Style Guide</a> is a good starting point when constructing messages that describe interactions.</p>

<h3>Culturally specific references</h3>

<p>It’s generally a good idea to stop using culturally specific references altogether, particularly if contrived, niche and to make understood. Always opt for clarity over clever, witty, puns.</p>

<h3>Emoji</h3>

<p>We need to be a friend 'on the shoulder' but not a friend getting in the way, irking or alienating the user. It is for this reason that we generally apply emoji as sprinkles of extra delight, more frequently in our products aimed at smaller businesses.</p>

<p>We should never replace conventional words with emoji - always augment by presenting these alongside text.</p>

<p>We should use emojis that can be easily interpreted and widely displayed.</p>

<h3>When choosing words...</h3>

<ul class="standard-list">
  <li>Keep things consistent. Be mindful of how concepts and functions have been described elsewhere - in a task flow, in the wider product, in support and marketing collateral. Use like phrases for like things unless there's a good reason not to.</li>
  <li>Aid readability by keeping things conversational. Using contractions or using a linking word like 'and', 'so', 'but' or 'because' can help. We do this all the time in speech and using them in written text can make our messaging sound more natural.</li>
  <li>Useless generalities should be avoided. How tall is high, how little is small, how big is large etc. Make these specific, relevant, terms rooted in a shared reality instead.</li>
  <li>Avoid idioms - Use clearly, widely understood, not easily confused language rather than witty phrases for which a user is either in on the joke or excluded.</li>
  <li>Don’t use common words in new ways - always opt for easily understood language that your customer both uses and understands.</li>
  <li>Care should be taken when choosing words to ensure that the inference / connotation does not outweigh the actual severity / meaning that’s intended.</li>
  <li>Offer a retry option only if there is the legitimate possibility of resolution in trying again. (It’s better to cut your losses if there’s the risk of further frustrations.)</li>
  <li>Keep legal language to a minimum - explain what you mean instead. Archaic jargon gets in the way of clarity and is fusty.</li>
</ul>

<h4><strong>Words to limit or avoid</strong></h4>

<ul class="standard-list">
  <li>Think carefully about Sorry. Use apologies, and words like 'sorry' sparingly and only when there are serious, impactful problems (including, but not limited to, data loss, being locked out of an account) to be conveyed. Do not use apologies for what is the expected operation of the program - while limitations may not be ideal we should seek to improve this (where we can) instead. Apologising constantly undermines confidence in the product, and, if these apologies persist, will annoy.</li>
  <li>Don't use 'Oops!'. The word has connotations of carelessness and making light of a situation or being flippant. It has potential to be overused and can annoy if repetitive or impactful. It can seem too playful at a time of user friction and annoyance.</li>
  <li>Limit using 'Please'. Confine to situations where the user is asked to do something inconvenient (such as waiting) or the software is otherwise to blame for an inconvenience.</li>
  <li>Limit using 'must'. Just tell the user what they need to do. It's implicit that if you're instructing someone then they should follow. Must can be used to convey the most essential of requirements where not following the instruction will be impactful or cause a flow to come to a halt. Always use 'must' over 'shall'.</li>
  <li>Limit using 'almost' or 'about'. This is too vague and can cause doubt and confusion. Be more specific.</li>
  <li>Limit using 'never'. Users are trying to do something in the moment so just tell them what to do (or not to do). There's no need to make the prohibition extend beyond the task at hand (even if it does).</li>
</ul>
