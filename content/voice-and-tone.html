---
layout: default
secondary_menu_id: content-voice
primary_menu_id: content
title: Voice &amp; Tone
---

<p class="summary">Here's how we want to be perceived, and how we can use content to create an appropriate and engaging personality with a consistent voice and tone.</p>

<h2>What is our personality?</h2>

<p>Principles make Sage's high-level aspirations real, and are framed by a brand, design or content creation lense.</p>

<p>Our principles, in turn, inform a personality - how we act and how we express ourselves. Our personality is:</p>

<ul class="standard-list">
  <li><strong>Straightforward</strong></li>
  <li><strong>Friendly</strong></li>
  <li><strong>Supportive</strong></li>
</ul>

<h2>What is our voice?</h3>

<p>Expanding the personality facets we begin to see how this can be applied to content - as our 'voice':</p>

<h4>Straightforward</h4>

<p>We say what we mean, and we mean what we say. We get to the point, cutting the waffle and unqualified hyperbole. We communicate concisely and clearly, with purpose.</p>

<h4>Friendly</h4>

<p>We're not stuffy, we talk and act as a friend would. We're welcoming, inclusive and engaging, whether you're just starting out as a beginner or expert. We don't get in the way.</p>

<h4>Supportive</h4>

<p>We're on our user's side, through thick and thin. We empathise with frustration and tedium and do all we can to support and encourage. We're not afraid to point out better ways of doing things or the consequences of a user's actions.</p>

<p>Our personality has informed our writing guidelines - how we bring the 'voice' of Sage Business Cloud to life, and how we use the components within the Sage Design System to communicate clearly, concisely, and usefully with users.</p>

<h3>How do we create content in the right voice?</h3>

<p>Thinking back to our design principles, these are the qualities we're looking for our straightforward, friendly, and supportive, Sage Business Cloud voice to convey. Here's some examples of how we can design content with this voice in mind...</p>

<table class="dls-table">
  <tbody>
    <tr>
      <td>
        <p><strong>&nbsp;</strong></p>
      </td>
      <td>
        <p><strong>A customer may feel...</strong></p>
      </td>
      <td>
        <p><strong>Qualities we want to convey to counter these feelings...</strong></p>
      </td>
      <td>
        <p><strong>What this looks like in practice...</strong></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><strong>1 - Be clear, and guide.</strong><strong><br /></strong><strong><br /></strong><strong>Show and guide to reassure users so that they know where they are, how they got there and where to go next.</strong><strong><br /></strong><strong><br /></strong><strong>Write sparingly (every word counts). &nbsp;Use familiar words and avoid extraneous info. What you remove or shorten is as important as what remains. </strong></p>
      </td>
      <td>
        <p><em><span style="font-weight: 400;">Unsure, Apprehensive, Unclear, Unconfident, Nervous, Lost, Confused.</span></em></p>
      </td>
      <td>
        <p><em><span style="font-weight: 400;">Mentoring, Helpful, Personal, Committed, Clear, Consistent, Friendly, Reassuring, Human, Real, Relevant, Concise, Smart, Minimal, Skimmable.</span></em></p>
      </td>
      <td>
        <p><span style="font-weight: 400;">Content should be relevant to context. </span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Knowing who are customers are, and what they need to achieve, should inform what we tell them and what options we present.</span></p>
        <br />
        <p><span style="font-weight: 400;">Leaving out extraneous information.</span></p>
        <p><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Telling customers what their next option(s) are.</span></p>
        <br />
        <p><span style="font-weight: 400;">Orientating customers appropriately in a flow, giving an understanding of total length, and the benefit they will get at the end.</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Encouraging along the way to build confidence.</span></p>
        <br />
        <p><span style="font-weight: 400;">Making calls to actions prominent and clear.</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Explaining what happened and why (if helpful) and how to fix things.</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Providing extra guidance for those new to our product, or when users encounter new features.</span></p>
        <br />
        <p><span style="font-weight: 400;">Making help easy to obtain at any time.</span></p>
        <br />
        <p><span style="font-weight: 400;">Using short sentences with familiar words.</span></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><strong>2 - Reassure, and be seen as trustworthy.</strong><strong><br /></strong><strong><br /></strong><strong>Talk directly to customers in a caring, friendly tone. Be conversational and nurture trust by making customers the centre of attention.</strong><strong><br /><br /></strong></p>
      </td>
      <td>
        <p><em><span style="font-weight: 400;">Neglected, Alone, Voiceless, Unacknowledged, Unsure, Doubtful, Unconvinced.</span></em></p>
      </td>
      <td>
        <p><em><span style="font-weight: 400;">Trusting, Confident, Warm, Content Supported, Trustworthy, Honest, Reassuring, Informal, Conversational, Engaging, Real, Empathetic, Friendly, Personable, Convinced.</span></em></p>
      </td>
      <td>
        <p><span style="font-weight: 400;">Showing customers that we know them and their circumstances.</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Addressing the customer as &lsquo;you&rsquo; or use &lsquo;we&rsquo; where appropriate for product / Sage.</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Writing as you speak - using contractions, common phrases, interjections, even dangling prepositions</span><span style="font-weight: 400;">.</span></p>
        <br />
        <p><span style="font-weight: 400;">Recognising the customer&rsquo;s point of view - what matters to the customer and how they are likely to be feeling.</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Staying focussed on what a customer is likely to want to do in a given situation (and convey this appropriately in messaging, animation, action etc.).</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Being honest and transparent - not trying to hide bad news or being overly optimistic. (Being mindful of the context and severity of the message).</span></p>
        <br />
        <p><span style="font-weight: 400;">Always being clear about who is doing what and ensure we adopt the the &lsquo;active voice&rsquo;.</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Actions should have clear effects (and clearly defined effects). Don&rsquo;t leave the audience not being sure if it&rsquo;s them or us making the decision or hide behind this (deliberately or not).</span></p>
        <br />
        <p><span style="font-weight: 400;">Apologise when appropriate but be sincere. (ie. Don&rsquo;t over apologise or apologise for non-impactful events).</span></p>
        <br />
        <p><span style="font-weight: 400;">Make experiences personal. i.e. Use real customer info, where appropriate, and use knowledge of the customer&rsquo;s situation, products, integrations, use of software features to drive promotions.</span></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><strong>3 - Be adaptive, and contextual.</strong></p>
      </td>
      <td>
        <p><em><span style="font-weight: 400;">Frustrated, Irrelevant, Overwhelmed, Confused.</span></em></p>
      </td>
      <td>
        <p><em><span style="font-weight: 400;">Understanding, Knowledgeable, Relevant</span></em></p>
      </td>
      <td>
        <p><span style="font-weight: 400;">Make experiences personal. Use real customer info, where appropriate, and use knowledge of the customer&rsquo;s situation, products, integrations, use of software features to drive promotions.</span></p>
        <br />
        <p><span style="font-weight: 400;">Providing extra guidance for those new to our product, or when users encounter new features or situations.</span></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><strong>4 - Be useful, and delightful. </strong><strong><br /></strong><strong><br /></strong><strong>Engage customers with playful language where appropriate, celebrate success and inject excitement and energy. Share and celebrate success.</strong></p>
      </td>
      <td>
        <p><em><span style="font-weight: 400;">Neglected, Alone, Voiceless, Unacknowledged.</span></em></p>
      </td>
      <td>
        <p><em><span style="font-weight: 400;">Trusting, Confident, Warm, Content Supported.</span></em></p>
      </td>
      <td>
        <p><span style="font-weight: 400;">Make experiences personal. Use real customer info, where appropriate, and use knowledge of the customer&rsquo;s situation, products, integrations, use of software features to drive promotions.</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Encouraging along the way to build customer confidence - to the point of cheering success.</span></p>
        <p><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Be clear on the effect of actions and purpose of functions to ensure users can understand these.</span></p>
        <br />
        <p><span style="font-weight: 400;">Use words and pictures, where appropriate to provide a &lsquo;human&rsquo; approach.</span></p>
      </td>
    </tr>
  </tbody>
</table>

<h2>What is our tone?</h2>

<p>Tone (how we say things, and what we sound like) can be turned up and down, on a spectrum that ranges from serious or matter-of fact to playful.</p>

<h3>Context and audience</h3>

<p>Context and audience are the biggest influences on what the Sage Business Cloud voice sounds like across a range of scenarios.</p>

<h4>Context</h4>

<p>Context is by far the biggest influence. For example, a regularly-occuring error message on how to put things right will be irksome. In this context, we really shouldn't be making light of things. Using 'Uh-Oh!' or 'Oops!' isn't an appropriate tone. We aim to guide and reassure and these words aren't in keeping.</p>

<h4>Audience</h4>

<p>We should also be mindful of audience. Remember that, in striving to be clear, concise and useful we're not dumbing down, but opening up. Even expert, enterprise users benefit from this clarity and brevity. We can, however, more confidently use accounting terminology and rely on a more detailed understanding of accounting concepts, and the effects of actions, with an expert or enterprise audience if we see advantage in doing so.</p>

<h3>How does this apply to audience segments?</h3>

<p>Here's how each of these segments can be represented in the language used and how we might explore writing for these segments, mindful of context.</p>

<h4>When writing for <strong>small</strong>:</h4>

<p>Be friendly. Be playful (in moderation). Be colloquial (where appropriate). Explain accounting terms on first use then contract / simplify (if appropriate, for reasons of space or clarity, for example).</p>


<h4>When writing for <strong>medium</strong>:</h4>
<p>As for small, but less colloquial.  Be playful (if appropriate). Simplify (if appropriate).</p>

<h4>When writing for <strong>enterprise</strong>:</h4>
<p>Don't be overly colloquial, rarely be playful, be simple, but don't overly simplify.</p>

<h4>When writing for <strong>any segments</strong>:</h4>

<p>Prioritise clarity. Ensure information meets the circumstances. Use sector specific language, suitably explained. Don't be afraid to be playful or delightful if the circumstances warrant and won't annoy.</p>

<p>Here's what that might look like in practice, mindful of types of user:</p>

<h4>New users</h4>

<p>Can be intimidated. They appreciate approachable, relatable, step-by-step introductions to terms and concepts to aid understanding and kickstart progress.</p>

<h4>Users</h4>

<p>Want to get stuff done, understand, and like encouragement - the occasional 'You did it!' or 'Good job!' can work well.</p>

<h4>Super users, Frequent users, and Developers</h4>

<p>Would prefer us to get to the point. They like it when we're upfront about limitations or pitfalls.</p>

<h4>All users</h4>

<p>Don't have time for lots of text. They need to get in, find what they need with no fuss, and get right back to their business or lives. Even expert, knowledgeable users will, therefore, favour brief, simple, Plain English.</p>

<h2>When applying voice and tone...</h2>

<ul class="standard-list">
  <li>Always be mindful as to the expectations and conventions your audience will expect.</li>
  <li>Question any assumptions and aim for consistency (though not necessarily parity) throughout product, support and marketing materials.</li>
  <li>When applying tone to messages always speak phrases (ideally out loud, at least in your head) as it serves as a good way of testing how messaging sounds as friendly conversation. This, in turn, ensures the phrases are easily understood and taken onboard).</li>
  <li>Keeping key messages clear of wordplay and adding a short extra phrase can be a good technique to keep the balance right. For example, 'Your first invoice has been created. Good job!'. Here the important information is provided upfront, but there's a friendly and enthusiastic tone applied at the end. The doesn't get in the way of what's most important - the meaning and can easily be skipped by someone scanning the message and doesn't slow them down.</li>
  <li>Always favour clarity, simplicity and consistency over ‘being clever’ with tone. (Clarity trumps wordplay every time). When considering surprising or delighting users, and in particular when applying humour, do this sparingly and appropriately.</li>
  <li>We should act as a 'friend on the shoulder', not a friend who gets in the way, irking the user as we deliver information. If in doubt, err on the side of caution and opt for a clear and straightforward message instead.</li>
  <li>Above all, in messaging - show the user what's happened, how to rectify (and if there's space and it's appropriate) why what's happened has happened. Get out of the way allowing the user to easily and quickly progress.</li>
</ul>
