FROM ruby:2.5

RUN apt-get update
RUN apt-get -qq update
RUN apt-get install -y nodejs

ENV APP_HOME /usr/src/app/
RUN mkdir -p $APP_HOME
WORKDIR $APP_HOME

COPY Gemfile $APP_HOME
COPY Gemfile.lock $APP_HOME

RUN bundle install

COPY . $APP_HOME